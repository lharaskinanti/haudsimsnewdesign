# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/local/FORTYTWO/franco.manghi/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep public class com.fortytwo.stu.android.MainActivity
-keep public class com.fortytwo.stu.android.receivers.SmsReceiver
-keep public class com.fortytwo.stu.android.receivers.MmsReceiver
-keep public class com.fortytwo.stu.android.ComposeSmsActivity
-keep public class com.fortytwo.stu.android.services.HeadlessSmsSendService
-keep public class com.fortytwo.stu.android.receivers.BootReceiver
-keep public class com.fortytwo.stu.android.receivers.AlarmReceiver

-assumenosideeffects class android.util.Log {
#   public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
#    public static int i(...);
#    public static int w(...);
    public static int d(...);
#    public static int e(...);
}

## Gson conf provided from: https://github.com/google/gson/blob/master/examples/android-proguard-example/proguard.cfg
##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

##---------------End: proguard configuration for Gson  ----------