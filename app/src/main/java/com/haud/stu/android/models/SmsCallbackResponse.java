package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class SmsCallbackResponse {

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("response_id")
    private String responseId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(final String responseId) {
        this.responseId = responseId;
    }
}