package com.haud.stu.android.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.haud.stu.android.SmsFromOa;
import com.haud.stu.android.db.DBHelper;
import com.haud.stu.android.db.DBModel;
import com.haud.stu.android.models.SmsFromInbox;
import com.haud.stu.android.models.SmsFromStorage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SmslistsUtils {


    public static List<SmsFromStorage> listStorageSMSes(final Context context, final int slot) {
        System.out.println("Trying to list all sms");
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        Cursor c = null;
        List<SmsFromStorage> smses = new ArrayList<>();
        //Set<SmsFromStorage> smses = new HashSet<>();
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getReadableDatabase();
            c = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` = %s GROUP BY `%s` ",
                    DBModel.SMSEntry.TABLE_NAME, DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT, slot,
                    DBModel.SMSEntry.COLUMN_NAME_ADDRESS), null);
            while (c.moveToNext()) {

                List<Integer> pdus = new ArrayList<>();

                long sms_entry_id = c.getLong(c.getColumnIndex(DBModel.SMSEntry._ID));
                long timestamp = c.getLong(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_TIMESTAMP));
                String event_uuid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID));
                String pdu_format = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_PDU_FORMAT));
                String sim_slot = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT));
                String sim_imsi = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_IMSI));
                String address = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_ADDRESS));
                String body = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_BODY));

                Cursor cPdus = null;
                try {
                    cPdus = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` = ?",
                            DBModel.SMSEntryPdu.TABLE_NAME, DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID),
                            new String[]{String.valueOf(sms_entry_id)});
                    while (cPdus.moveToNext()) {
                        pdus.add(cPdus.getColumnIndex(DBModel.SMSEntryPdu.COLUMN_NAME_PDU));
                    }
                } finally {
                    if (cPdus != null) {
                        cPdus.close();
                    }
                }
                SmsFromStorage objt = new SmsFromStorage();

                objt.setTimestamp(timestamp);
                objt.setEvent_uuid(event_uuid);
                objt.setPdu_format(pdu_format);
                objt.setSim_slot(sim_slot);
                objt.setSim_imsi(sim_imsi);
                objt.setSms_entry_id(sms_entry_id);
                objt.setPdu(pdus);
                objt.setAddress(address);
                objt.setBody(body);
                smses.add(objt);
            }
        } finally {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }

        return smses;
    }


    public static List<SmsFromInbox> getSmsInbox(Context ctx){
        List<SmsFromInbox> sms = new ArrayList<>();
        Uri uriSMSURI = Uri.parse("content://sms/inbox");
        Cursor cur = ctx.getContentResolver().query(uriSMSURI, null, null, null, null);
        for (String s : cur.getColumnNames()){
            Log.d("COLUMN_NAME", s);}

        while (cur != null && cur.moveToNext()) {
            String address = cur.getString(cur.getColumnIndex("address"));
            String body = cur.getString(cur.getColumnIndexOrThrow("body"));
            String imsi = cur.getString(cur.getColumnIndexOrThrow("sim_imsi"));
            String slot = cur.getString(cur.getColumnIndexOrThrow("sim_slot"));
            String threadId  = cur.getString(cur.getColumnIndexOrThrow("thread_id"));

            SmsFromInbox objt = new SmsFromInbox();
            objt.setSim_slot(slot);
            objt.setBody(body);
            objt.setAddress(address);
            objt.setSim_imsi(imsi);
            objt.setThreadId(threadId);
            sms.add(objt);
        }

        if (cur != null) {
            cur.close();
        }
        //System.out.println(cur);
        return sms;
    }


    public static List<SmsFromStorage> listStorageSMSesbyAddress(final Context context,
                                                                 final String addressGroup, int slot) {
        System.out.println("Trying to list all sms from " + addressGroup);
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        Cursor c = null;
        List<SmsFromStorage> smses = new ArrayList<>();
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getReadableDatabase();

            String query = "SELECT * FROM `sms_entry` where `address` LIKE '%"+addressGroup+"%' AND `sim_slot` = "+slot+" ";

            c = db.rawQuery(query, null);

//            c = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` LIKE ?",
//                    DBModel.SMSEntry.TABLE_NAME, DBModel.SMSEntry.COLUMN_NAME_ADDRESS, new String[]{"%"+addressGroup+"%"}), null);
            while (c.moveToNext()) {

                List<Integer> pdus = new ArrayList<>();

                long sms_entry_id = c.getLong(c.getColumnIndex(DBModel.SMSEntry._ID));
                long timestamp = c.getLong(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_TIMESTAMP));
                String event_uuid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID));
                String pdu_format = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_PDU_FORMAT));
                String sim_slot = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT));
                String sim_imsi = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_IMSI));
                String address = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_ADDRESS));
                String body = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_BODY));
                Integer status = c.getInt(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_STATUS));

                Cursor cPdus = null;
                try {
                    cPdus = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` = ?",
                            DBModel.SMSEntryPdu.TABLE_NAME, DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID),
                            new String[]{String.valueOf(sms_entry_id)});
                    while (cPdus.moveToNext()) {
                        pdus.add(cPdus.getColumnIndex(DBModel.SMSEntryPdu.COLUMN_NAME_PDU));
                    }
                } finally {
                    if (cPdus != null) {
                        cPdus.close();
                    }
                }
                SmsFromStorage objt = new SmsFromStorage();

                objt.setTimestamp(timestamp);
                objt.setEvent_uuid(event_uuid);
                objt.setPdu_format(pdu_format);
                objt.setSim_slot(sim_slot);
                objt.setSim_imsi(sim_imsi);
                objt.setSms_entry_id(sms_entry_id);
                objt.setPdu(pdus);
                objt.setAddress(address);
                objt.setBody(body);
                objt.setStatus(status);
                smses.add(objt);
            }
        } finally {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
        return smses;
    }


}
