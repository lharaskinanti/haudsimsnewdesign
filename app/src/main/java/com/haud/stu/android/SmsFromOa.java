package com.haud.stu.android;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.shape.MaterialShapeDrawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.haud.stu.android.models.SmsFromStorage;
import com.haud.stu.android.utils.SmslistsUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class SmsFromOa extends AppCompatActivity {

    private Object Utils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_from_oa);
        final String from = getIntent().getStringExtra("from");
        final int slot = getIntent().getIntExtra("slot", 0);

        TextView addressTitle = (TextView) findViewById(R.id.addressTitle);
        addressTitle.setText(from);

        final List<SmsFromStorage> smses = SmslistsUtils.listStorageSMSesbyAddress(this, from, slot);
        System.out.println(smses);
        ArrayAdapter<SmsFromStorage> adapter = new smsFromOaAdapter(this, 0, (ArrayList<SmsFromStorage>) smses);

        ListView listView = (ListView) findViewById(R.id.sfoListView);
        listView.setAdapter(adapter);

        TextView pushButton = (TextView) findViewById(R.id.pushButton);
        pushButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushPendingSms();
                finish();
                startActivity(getIntent());
            }
        });

        ImageView backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setResult(SmsFromOa.RESULT_OK);
    }

    public void pushPendingSms() {
        final Context ctx = this;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    System.out.println("pushPendingSMSes");
                    SMSManager.pushPendingSMSes(ctx);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}


class smsFromOaAdapter extends ArrayAdapter<SmsFromStorage> {
    private Context context;

    private static class ViewHolder {
        private TextView itemView;
    }

    public smsFromOaAdapter(Context context, int resource, ArrayList<SmsFromStorage> objects) {
        super(context, resource, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SmsFromStorage inboxList = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(this.getContext());

        View view;
        view = inflater.inflate(R.layout.messages, null);

        TextView description = (TextView) view.findViewById(R.id.messages);
        TextView datetime = (TextView) view.findViewById(R.id.datetime);

        ImageView logo = (ImageView) view.findViewById(R.id.address_logo);


        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, //set a gradient direction
                new int[] {Color.BLUE, Color.GRAY}); //set the color of gradient
        gradientDrawable.setCornerRadius(50f);

        GradientDrawable drawable = new GradientDrawable();
        drawable.setCornerRadius( 8 );
        drawable.setColor(Color.parseColor("#eff0f1"));



        description.setBackground(drawable);
        logo.setBackground(gradientDrawable);


        Long milliseconds = inboxList.getTimestamp();

//        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);
//        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);

        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);

        datetime.setText(String.valueOf(hours) + ":" + String.valueOf(minutes));

        ImageView mImageView = (ImageView) view.findViewById(R.id.status);

        System.out.println("getStatus " + inboxList.getStatus());
        if(inboxList.getStatus() == 1) {
            mImageView.setImageResource(R.drawable.tick_mark_icon);
        } else {
            mImageView.setImageResource(R.drawable.pending_line);
        }

        description.setText(inboxList.getBody());

        return view;
    }
}

