package com.haud.stu.android.listeners;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class LocationListener {
    private static Location latestLocation = null;
    private static final AtomicBoolean INITIALISED = new AtomicBoolean(false);

    public static synchronized void init(Context ctx, final Runnable updateOnNewFix) {
        if (INITIALISED.get()) {
            return;
        }
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Log.w(LocationListener.class.getSimpleName(), "Missing location permissions.");
            return;
        }
        final LocationManager locationManager = (LocationManager)
                ctx.getSystemService(Context.LOCATION_SERVICE);
        android.location.LocationListener listener = new android.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latestLocation = location;
                if(updateOnNewFix != null){
                    updateOnNewFix.run();
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
        INITIALISED.set(true);
    }

    public static Location getLatestLocation(final Context ctx) {
        if(latestLocation != null){
            return latestLocation;
        }
        return determineLastKnownLocation(ctx);
    }

    private static Location determineLastKnownLocation(final Context ctx) {
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Log.w(LocationListener.class.getSimpleName(), "Missing location permissions.");
            return null;
        }
        final LocationManager locationManager = (LocationManager)
                ctx.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Log.d(LocationListener.class.getSimpleName(), String.format("Found %s location providers.",
                providers == null ? null : providers.size()));
        Location bestLocation = null;
        for (String provider : providers) {
            Log.d(LocationListener.class.getSimpleName(),
                    String.format("Found location provider [%s] with requires cell [%b] and " +
                            "requires network [%b] and requires satellite [%b].", provider,
                            locationManager.getProvider(provider).requiresCell(),
                            locationManager.getProvider(provider).requiresNetwork(),
                            locationManager.getProvider(provider).requiresSatellite()));
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                Log.d(LocationListener.class.getSimpleName(),
                        String.format("Location provider [%s] is null.", provider));
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                Log.d(LocationListener.class.getSimpleName(),
                        String.format("Location provider [%s] has accuracy of: %s.", provider,
                                l.getAccuracy()));
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public static boolean isInitialised() {
        return INITIALISED.get();
    }

    public static boolean isLocationServiceEnabled(Context ctx){
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Log.w(LocationListener.class.getSimpleName(), "Missing location permissions.");
            return false;
        }
        LocationManager lm = (LocationManager)ctx.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Log.d(LocationListener.class.getSimpleName(), String.format("GPS Location: [%b] - Network " +
                "Location: [%b]", gpsEnabled, networkEnabled));
        return gpsEnabled && networkEnabled;
    }
}
