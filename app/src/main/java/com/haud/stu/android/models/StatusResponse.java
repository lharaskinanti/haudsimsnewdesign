package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class StatusResponse {

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("response_id")
    private String responseId;
    @SerializedName("block_operation")
    private boolean blockOperation;
    @SerializedName("sms_message_stu_filters")
    private Set<String> smsMessageStuFilters;

    public StatusResponse() {
    }

    public StatusResponse(final String requestId, final String responseId,
                          final boolean blockOperation) {
        this.requestId = requestId;
        this.responseId = responseId;
        this.blockOperation = blockOperation;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(final String responseId) {
        this.responseId = responseId;
    }

    public boolean isBlockOperation() {
        return blockOperation;
    }

    public void setBlockOperation(final boolean blockOperation) {
        this.blockOperation = blockOperation;
    }

    public Set<String> getSmsMessageStuFilters() {
        return smsMessageStuFilters;
    }

    public void setSmsMessageStuFilters(final Set<String> smsMessageStuFilters) {
        this.smsMessageStuFilters = smsMessageStuFilters;
    }

    @Override
    public String toString() {
        return "StatusResponse{" +
                "requestId='" + requestId + '\'' +
                ", responseId='" + responseId + '\'' +
                ", blockOperation=" + blockOperation +
                ", smsMessageStuFilters=" + smsMessageStuFilters +
                '}';
    }
}