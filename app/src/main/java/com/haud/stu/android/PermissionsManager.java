package com.haud.stu.android;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.haud.stu.android.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PermissionsManager {
    private static HashMap<Integer, String> NEEDED_PERMISSIONS = new HashMap<>();
    private static HashMap<String, Integer> PERMISSIONS_MAP = new HashMap<>();

    static {
        NEEDED_PERMISSIONS.put(1, Manifest.permission.RECEIVE_SMS);
        NEEDED_PERMISSIONS.put(2, Manifest.permission.READ_PHONE_STATE);
        NEEDED_PERMISSIONS.put(3, Manifest.permission.ACCESS_NETWORK_STATE);
        NEEDED_PERMISSIONS.put(4, Manifest.permission.ACCESS_WIFI_STATE);
        NEEDED_PERMISSIONS.put(5, Manifest.permission.ACCESS_COARSE_LOCATION);
        NEEDED_PERMISSIONS.put(6, Manifest.permission.ACCESS_FINE_LOCATION);
        NEEDED_PERMISSIONS.put(7, Manifest.permission.RECEIVE_BOOT_COMPLETED);
        NEEDED_PERMISSIONS.put(8, Manifest.permission.INTERNET);
        NEEDED_PERMISSIONS.put(9, Manifest.permission.READ_SMS);
        PERMISSIONS_MAP.put(Manifest.permission.RECEIVE_SMS, 1);
        PERMISSIONS_MAP.put(Manifest.permission.READ_PHONE_STATE, 2);
        PERMISSIONS_MAP.put(Manifest.permission.ACCESS_NETWORK_STATE, 3);
        PERMISSIONS_MAP.put(Manifest.permission.ACCESS_WIFI_STATE, 4);
        PERMISSIONS_MAP.put(Manifest.permission.ACCESS_COARSE_LOCATION, 5);
        PERMISSIONS_MAP.put(Manifest.permission.ACCESS_FINE_LOCATION, 6);
        PERMISSIONS_MAP.put(Manifest.permission.RECEIVE_BOOT_COMPLETED, 7);
        PERMISSIONS_MAP.put(Manifest.permission.INTERNET, 8);
        PERMISSIONS_MAP.put(Manifest.permission.READ_SMS, 9);
    }

    public static void checkPermissions(final Activity activity) {
        final List<String> unsatisfiedPermissions = new ArrayList<>();
        Set<Map.Entry<Integer, String>> perms = NEEDED_PERMISSIONS.entrySet();
        for (Map.Entry<Integer, String> perm : perms) {
            if (ContextCompat.checkSelfPermission(activity, perm.getValue())
                    != PackageManager.PERMISSION_GRANTED) {
                unsatisfiedPermissions.add(perm.getValue());
            }
        }
        if (!unsatisfiedPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(activity, unsatisfiedPermissions.toArray(
                    new String[unsatisfiedPermissions.size()]), 1);
        } else {
            STUAlarmManager.init(activity);
        }
    }

    public static void handlePermissionsResult(final int requestCode, final String permissions[],
                                               final int[] grantResults, final Activity activity) {
        final List<String> failed = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                failed.add(permissions[i]);
            }
        }
        if (failed.isEmpty()) {
            activity.finish();
            activity.startActivity(activity.getIntent());
            return;
        }
        final StringBuilder errMsg = new StringBuilder();
        errMsg.append(activity.getString(R.string.missing_permissions_message));
        for (final String perm : failed) {
            errMsg.append("- ");
            errMsg.append(perm);
            errMsg.append("\n");
        }
        new AlertDialog.Builder(activity).setTitle(activity.getString(R.string.missing_permissions_title))
                .setMessage(errMsg).setPositiveButton(activity.getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkPermissions(activity);
            }
        }).setNegativeButton(activity.getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(-1);
            }
        }).setIcon(R.drawable.ic_alert).setCancelable(false).show();
    }
}
