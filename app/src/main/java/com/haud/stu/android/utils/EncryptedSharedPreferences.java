package com.haud.stu.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class EncryptedSharedPreferences implements SharedPreferences {
    private final SharedPreferences sharedPref;
    private final String key;

    public EncryptedSharedPreferences(final Context ctx) {
        this.sharedPref = ctx.getSharedPreferences(String.format("%s.PREFS",
                ctx.getPackageName().toUpperCase()), Context.MODE_PRIVATE);
        key = generateSecretKey(ctx);
    }

    public static class EncryptedEditor implements Editor {
        private final Editor editor;
        private final String key;

        public EncryptedEditor(final Editor editor, final String key) {
            this.editor = editor;
            this.key = key;
        }

        @Override
        public Editor putString(String key, String value) {
            return editor.putString(encrypt(key), encrypt(value));
        }

        @Override
        public Editor putStringSet(String key, Set<String> values) {
            if (values == null) {
                return editor.putStringSet(encrypt(key), values);
            }
            final Set<String> encrypted = new LinkedHashSet<>(values.size());
            for (String s : values) {
                encrypted.add(encrypt(s));
            }
            return editor.putStringSet(encrypt(key), encrypted);
        }

        @Override
        public Editor putInt(String key, int value) {
            return editor.putString(encrypt(key), encrypt(value));
        }

        @Override
        public Editor putLong(String key, long value) {
            return editor.putString(encrypt(key), encrypt(value));
        }

        @Override
        public Editor putFloat(String key, float value) {
            return editor.putString(encrypt(key), encrypt(value));
        }

        @Override
        public Editor putBoolean(String key, boolean value) {
            return editor.putString(encrypt(key), encrypt(value));
        }

        @Override
        public Editor remove(String key) {
            return editor.remove(encrypt(key));
        }

        @Override
        public Editor clear() {
            return editor.clear();
        }

        @Override
        public boolean commit() {
            return editor.commit();
        }

        @Override
        public void apply() {
            editor.apply();
        }
    }

    private String generateSecretKey(final Context ctx) {
        //deviceserial-primaryimei
        return String.format("%s-%s", Build.SERIAL, GeneralUtils.getDefaultImei(ctx));
    }

    @Override
    public Map<String, ?> getAll() {
        final Map<String, ?> result = sharedPref.getAll();
        if (result == null) {
            return null;
        }
        final Map<String, Object> decrypted = new HashMap<>();
        for (Map.Entry<String, ?> e : decrypted.entrySet()) {
            decrypted.put(decryptString(e.getKey()), decrypt((String) e.getValue()));
        }
        return decrypted;
    }

    @Nullable
    @Override
    public String getString(String key, String defValue) {
        final String result = sharedPref.getString(encrypt(key), null);
        if (result == null) {
            return defValue;
        }
        return decryptString(result);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        final Set<String> result = sharedPref.getStringSet(encrypt(key), null);
        if (result == null) {
            return defValues;
        }
        final Set<String> decrypted = new HashSet<>(result.size());
        for (String s : result) {
            decrypted.add(decryptString(s));
        }
        return decrypted;
    }

    @Override
    public int getInt(String key, int defValue) {
        final String result = sharedPref.getString(encrypt(key), null);
        if (result == null) {
            return defValue;
        }
        return decryptInteger(result);
    }

    @Override
    public long getLong(String key, long defValue) {
        final String result = sharedPref.getString(encrypt(key), null);
        if (result == null) {
            return defValue;
        }
        return decryptLong(result);
    }

    @Override
    public float getFloat(String key, float defValue) {
        final String result = sharedPref.getString(encrypt(key), null);
        if (result == null) {
            return defValue;
        }
        return decryptFloat(result);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        final String result = sharedPref.getString(encrypt(key), null);
        if (result == null) {
            return defValue;
        }
        return decryptBoolean(result);
    }

    @Override
    public boolean contains(String key) {
        return sharedPref.contains(encrypt(key));
    }

    @Override
    public Editor edit() {
        return new EncryptedEditor(sharedPref.edit(), key);
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPref.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPref.unregisterOnSharedPreferenceChangeListener(listener);
    }

    private static String encrypt(final Object obj) {
//// TODO: 16/05/16
        return null;
    }

    private static Object decrypt(final String b64EncryptedVal) {
//// TODO: 16/05/16
        return null;
    }

    private static String decryptString(final String b64EncryptedVal) {
        final Object result = decrypt(b64EncryptedVal);
        return result == null ? null : (String) result;
    }

    private static Boolean decryptBoolean(final String b64EncryptedVal) {
        final Object result = decrypt(b64EncryptedVal);
        return result == null ? null : (boolean) result;
    }

    private static Integer decryptInteger(final String b64EncryptedVal) {
        final Object result = decrypt(b64EncryptedVal);
        return result == null ? null : (int) result;
    }

    private static Long decryptLong(final String b64EncryptedVal) {
        final Object result = decrypt(b64EncryptedVal);
        return result == null ? null : (long) result;
    }

    private static Float decryptFloat(final String b64EncryptedVal) {
        final Object result = decrypt(b64EncryptedVal);
        return result == null ? null : (float) result;
    }
}
