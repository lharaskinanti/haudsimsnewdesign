package com.haud.stu.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.haud.stu.android.ApiRequestManager;
import com.haud.stu.android.SMSManager;
import com.haud.stu.android.models.StatusResponse;

import java.util.UUID;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final PendingResult result = goAsync();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final String uuid = UUID.randomUUID().toString();
                Log.i(AlarmReceiver.class.getSimpleName(), String.format("[%s] - Alarm triggered - sending" +
                        " keep-alive.", uuid));
                final StatusResponse response = ApiRequestManager.submitStatusCallback(context, uuid);
                if(response != null) {
                    SMSManager.processPendingSMSes(context);
                }
                Log.d(AlarmReceiver.class.getSimpleName(), String.format("[%s] - Received response:%n%s",
                        uuid, response));
                result.finish();
            }
        });
    }
}