package com.haud.stu.android.listeners;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.haud.stu.android.R;
import com.haud.stu.android.utils.GeneralUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class PhoneSignalListener {
    private static final String KEY_SHARED_PREF = "com.fortytwo.stu.locationtracker";
    private static Map<Integer, SignalStrengthInfo> latestStrengthInfos = new HashMap<>();
    private static final AtomicBoolean INITIALISED = new AtomicBoolean(false);

    public static class SignalStrengthInfo {
        private final SignalStrength strength;
        private int signalLevel;
        private String signalType;

        public static final int SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;

        public SignalStrengthInfo(final SignalStrength strength, final Context ctx) {
            this.strength = strength;
            calcLevel();
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1){
                signalType = getNetworkTypeLegacy(ctx);
            }
        }

        public SignalStrengthInfo(final int signalLevel, final String signalType) {
            this.strength = null;
            this.signalLevel = signalLevel;
            this.signalType = signalType;
        }

        private static String getNetworkTypeLegacy(final Context context) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_NETWORK_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                return context.getString(R.string.unknown);
            }
            final TelephonyManager teleMan = (TelephonyManager)
                    context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = teleMan.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return "1xRTT";
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return "CDMA";
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return "EDGE";
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    return "eHRPD";
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return "EVDO rev. 0";
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return "EVDO rev. A";
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    return "EVDO rev. B";
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return "GPRS";
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return "HSDPA";
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return "HSPA";
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "HSPA+";
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return "HSUPA";
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "iDen";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "LTE";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return "UMTS";
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    return "Unknown";
                default:
                    return String.valueOf(networkType);
            }
        }

        public int getLteLevel() {
            try {
                Method m = SignalStrength.class.getMethod("getLteLevel");
                return (int) m.invoke(strength);
            } catch (final Exception e) {
                Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
            }
            return SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        }

        public int getTdScdmaLevel() {
            try {
                Method m = SignalStrength.class.getMethod("getTdScdmaLevel");
                return (int) m.invoke(strength);
            } catch (final Exception e) {
                Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
            }
            return SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        }

        public int getGsmLevel() {
            try {
                Method m = SignalStrength.class.getMethod("getGsmLevel");
                return (int) m.invoke(strength);
            } catch (final Exception e) {
                Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
            }
            return SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        }

        public int getCdmaLevel() {
            try {
                Method m = SignalStrength.class.getMethod("getCdmaLevel");
                return (int) m.invoke(strength);
            } catch (final Exception e) {
                Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
            }
            return SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        }

        public int getEvdoLevel() {
            try {
                Method m = SignalStrength.class.getMethod("getEvdoLevel");
                return (int) m.invoke(strength);
            } catch (final Exception e) {
                Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
            }
            return SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        }

        /**
         * Retrieve an abstract level value for the overall signal strength.
         *
         * @return a single integer from 0 to 4 representing the general signal quality.
         * This may take into account many different radio technology inputs.
         * 0 represents very poor signal strength
         * while 4 represents a very strong signal strength.
         */
        public int getLevel() {
            return signalLevel;
        }

        /**
         * Retrieve an abstract level value for the overall signal strength. This is a utility
         * method which all it does is calculate the value from the {@link #getLevel()} method by
         * simply multiplying that result (out of 4) by 25 to make it out of 100.
         *
         * @return a single integer from 0 to 100 representing the general signal quality.
         * This may take into account many different radio technology inputs.
         * 0 represents very poor signal strength
         * while 100 represents a very strong signal strength.
         */
        public int getLevelPercentage() {
            return signalLevel * 25;
        }

        public String getSignalType() {
            return signalType;
        }

        /**
         * Retrieve an abstract level value for the overall signal strength.
         *
         * @return a single integer from 0 to 4 representing the general signal quality.
         * This may take into account many different radio technology inputs.
         * 0 represents very poor signal strength
         * while 4 represents a very strong signal strength.
         */
        private void calcLevel() {
            if (strength == null) {
                signalLevel = 0;
                signalType = null;
                return;
            }
            int level = 0;
            String type = null;
            if (strength.isGsm()) {
                level = getLteLevel();
                type = "LTE";
                if (level == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                    level = getTdScdmaLevel();
                    type = "TD-SCDMA";
                    if (level == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                        level = getGsmLevel();
                        type = "GSM";
                    }
                }
            } else {
                int cdmaLevel = getCdmaLevel();
                int evdoLevel = getEvdoLevel();
                if (evdoLevel == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                /* We don't know evdo, use cdma */
                    level = cdmaLevel;
                    type = "CDMA";
                } else if (cdmaLevel == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                /* We don't know cdma, use evdo */
                    level = evdoLevel;
                    type = "EVDO";
                } else {
                /* We know both, use the lowest level */
                    level = cdmaLevel < evdoLevel ? cdmaLevel : evdoLevel;
                    type = level == cdmaLevel ? "CDMA" : "EVDO";
                }
            }
            signalLevel = level;
            signalType = type;
            return;
        }
    }

    public static synchronized void init(final Context ctx, final Runnable onNewSignalStrength,
                                         final int[] slots) {
        if (INITIALISED.get()) {
            return;
        }
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        List<PhoneStateListener> listeners = new ArrayList<>();
        for (final int slot : slots) {
            PhoneStateListener listener = new PhoneStateListener() {
                @Override
                public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                    super.onSignalStrengthsChanged(signalStrength);
                    SignalStrengthInfo ssi = new SignalStrengthInfo(signalStrength, ctx);
                    latestStrengthInfos.put(slot, ssi);
                    final SharedPreferences sharedPref = ctx.getSharedPreferences(
                            KEY_SHARED_PREF, Context.MODE_PRIVATE);
                    final SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(String.format("%d-STRENGTH", slot),
                            String.valueOf(ssi.getLevel()));
                    editor.putString(String.format("%d-TYPE", slot), ssi.getSignalType());
                    editor.commit();
                    if (onNewSignalStrength != null) {
                        onNewSignalStrength.run();
                    }
                }
            };
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    int phoneId = 0;
                    boolean ok = false;
                    try {
                        Method mPhoneId = SubscriptionManager.class.getMethod("getPhoneId", int.class);
                        phoneId = (int) mPhoneId.invoke(null, slot);
                        ok = true;
                    } catch (Exception e) {
                        Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
                    }
                    if (ok) {
                        Field f = PhoneStateListener.class.getDeclaredField("mSubId");
                        boolean accessable = f.isAccessible();
                        f.setAccessible(true);
                        f.set(listener, phoneId);
                        f.setAccessible(accessable);
                    }
                } catch (Exception e) {
                    Log.e(PhoneSignalListener.class.getSimpleName(), e.getMessage(), e);
                }
            }
            listeners.add(listener);
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        for (PhoneStateListener listener : listeners) {
            telephonyManager.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        }
        INITIALISED.set(true);
    }

    public static boolean isInitialised() {
        return INITIALISED.get();
    }

    public static Map<Integer, SignalStrengthInfo> getLatestStrengthInfos(Context ctx) {
        int[] subIds = GeneralUtils.getSubIds(ctx);
        Map<Integer, SignalStrengthInfo> result = new HashMap<>();
        for (int slot : subIds) {
            result.put(slot, getLatestStrengthInfo(slot, ctx));
        }
        return result;
    }

    public static SignalStrengthInfo getLatestStrengthInfo(int slotId, Context ctx) {
        final SharedPreferences sharedPref = ctx.getSharedPreferences(
                KEY_SHARED_PREF, Context.MODE_PRIVATE);
        String level = sharedPref.getString(String.format("%d-STRENGTH", slotId), null);
        String type = sharedPref.getString(String.format("%d-TYPE", slotId), null);
        return new SignalStrengthInfo(level == null ? 0 : Integer.parseInt(level), type);
    }
}
