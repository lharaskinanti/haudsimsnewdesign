package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class BaseEncryptedRequest {

    @SerializedName("symmetric_key")
    private String symmetricKey;
    @SerializedName("data")
    private String data;

    public BaseEncryptedRequest() {
    }

    public BaseEncryptedRequest(final String symmetricKey, final String data) {
        this.symmetricKey = symmetricKey;
        this.data = data;
    }

    public String getSymmetricKey() {
        return symmetricKey;
    }

    public void setSymmetricKey(final String symmetricKey) {
        this.symmetricKey = symmetricKey;
    }

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return String.format("BaseEncryptedRequest:[{symmetricKey}=>{%s}; {data}=>"
                + "{%s}]", symmetricKey, data);
    }

}