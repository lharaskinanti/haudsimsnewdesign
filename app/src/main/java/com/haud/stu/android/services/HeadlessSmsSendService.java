package com.haud.stu.android.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class HeadlessSmsSendService extends IntentService {

    public HeadlessSmsSendService() {
        super(HeadlessSmsSendService.class.getSimpleName());
        setIntentRedelivery(true);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        Log.i(HeadlessSmsSendService.class.getSimpleName(), String.format("onHandleIntent with " +
                "action [%s].", action));
    }
}
