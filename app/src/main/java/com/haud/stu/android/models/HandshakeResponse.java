package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class HandshakeResponse {

    @SerializedName("public_key")
    private String publicKey;

    public void setPublicKey(final String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPublicKey() {
        return publicKey;
    }
}