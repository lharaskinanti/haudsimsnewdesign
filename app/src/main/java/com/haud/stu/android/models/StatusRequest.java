package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class StatusRequest {

    public enum PowerMode {
        BATTERY, AC;
    }

    public static class DeviceInfo {

        @SerializedName("serial_number")
        private String serialNumber;
        @SerializedName("os")
        private String os;
        @SerializedName("os_version")
        private String osVersion;
        @SerializedName("os_api_level")
        private Double osApiLevel;
        @SerializedName("battery_level")
        private Double batteryLevel;
        @SerializedName("power_mode")
        private String powerModeStr;
        @SerializedName("model")
        private String model;
        @SerializedName("app_version")
        private Double appVersion;
        @SerializedName("app_version_string")
        private String appVersionStr;

        private transient PowerMode powerMode;

        public String getSerialNumber() {
            return serialNumber;
        }

        public String getOs() {
            return os;
        }

        public String getOsVersion() {
            return osVersion;
        }

        public Double getOsApiLevel() {
            return osApiLevel;
        }

        public Double getBatteryLevel() {
            return batteryLevel;
        }

        public String getPowerModeStr() {
            return powerModeStr;
        }

        public String getModel() {
            return model;
        }

        public Double getAppVersion() {
            return appVersion;
        }

        public PowerMode getPowerMode() {
            return powerMode;
        }

        public String getAppVersionStr() {
            return appVersionStr;
        }

        public void setSerialNumber(final String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public void setOs(final String os) {
            this.os = os;
        }

        public void setOsVersion(final String osVersion) {
            this.osVersion = osVersion;
        }

        public void setOsApiLevel(final Double osApiLevel) {
            this.osApiLevel = osApiLevel;
        }

        public void setBatteryLevel(final Double batteryLevel) {
            this.batteryLevel = batteryLevel;
        }

        public void setPowerModeStr(final String powerModeStr) {
            this.powerModeStr = powerModeStr;
        }

        public void setModel(final String model) {
            this.model = model;
        }

        public void setAppVersion(final Double appVersion) {
            this.appVersion = appVersion;
        }

        public void setPowerMode(final PowerMode powerMode) {
            this.powerMode = powerMode;
            this.powerModeStr = powerMode == null ? null : powerMode.toString();
        }

        public void setAppVersionStr(String appVersionStr) {
            this.appVersionStr = appVersionStr;
        }
    }

    public static class SIMCard {

        @SerializedName("slot")
        private Integer slot;
        @SerializedName("imei")
        private String imei;
        @SerializedName("imsi")
        private String imsi;
        @SerializedName("iccid")
        private String iccid;
        @SerializedName("mcc")
        private Integer mcc;
        @SerializedName("mnc")
        private Integer mnc;
        @SerializedName("carrier")
        private String carrier;
        @SerializedName("number")
        private String number;
        @SerializedName("roaming")
        private Boolean roaming;

        public Integer getSlot() {
            return slot;
        }

        public String getImei() {
            return imei;
        }

        public String getImsi() {
            return imsi;
        }
        public String getIccid() {
            return iccid;
        }

        public Integer getMcc() {
            return mcc;
        }

        public Integer getMnc() {
            return mnc;
        }

        public String getCarrier() {
            return carrier;
        }

        public String getNumber() {
            return number;
        }

        public Boolean getRoaming() {
            return roaming;
        }

        public void setSlot(final Integer slot) {
            this.slot = slot;
        }

        public void setImei(final String imei) {
            this.imei = imei;
        }

        public void setImsi(final String imsi) {
            this.imsi = imsi;
        }
        public void setIccid(final String iccid) {
            this.iccid = iccid;
        }

        public void setMcc(final Integer mcc) {
            this.mcc = mcc;
        }

        public void setMnc(final Integer mnc) {
            this.mnc = mnc;
        }

        public void setCarrier(final String carrier) {
            this.carrier = carrier;
        }

        public void setNumber(final String number) {
            this.number = number;
        }

        public void setRoaming(final Boolean roaming) {
            this.roaming = roaming;
        }
    }

    public static class WiFi {

        @SerializedName("enabled")
        private Boolean enabled;
        @SerializedName("signal_strength")
        private Double signalStrength;
        @SerializedName("ssid")
        private String ssid;

        public Boolean getEnabled() {
            return enabled;
        }

        public Double getSignalStrength() {
            return signalStrength;
        }

        public String getSsid() {
            return ssid;
        }

        public void setEnabled(final Boolean enabled) {
            this.enabled = enabled;
        }

        public void setSignalStrength(final Double signalStrength) {
            this.signalStrength = signalStrength;
        }

        public void setSsid(final String ssid) {
            this.ssid = ssid;
        }
    }

    public static class Network {

        @SerializedName("slot")
        private Integer slot;
        @SerializedName("signal_strength")
        private Double signalStrength;
        @SerializedName("network_type")
        private String networkType;
        @SerializedName("mcc")
        private Integer mcc;
        @SerializedName("mnc")
        private Integer mnc;

        public Integer getSlot() {
            return slot;
        }

        public Double getSignalStrength() {
            return signalStrength;
        }

        public String getNetworkType() {
            return networkType;
        }

        public Integer getMcc() {
            return mcc;
        }

        public Integer getMnc() {
            return mnc;
        }

        public void setSlot(final Integer slot) {
            this.slot = slot;
        }

        public void setSignalStrength(final Double signalStrength) {
            this.signalStrength = signalStrength;
        }

        public void setNetworkType(final String networkType) {
            this.networkType = networkType;
        }

        public void setMcc(final Integer mcc) {
            this.mcc = mcc;
        }

        public void setMnc(final Integer mnc) {
            this.mnc = mnc;
        }
    }

    public static class Location {

        @SerializedName("latitude")
        private Double latitude;
        @SerializedName("longitude")
        private Double longitude;
        @SerializedName("last_fix")
        private Long lastFix;

        public Double getLatitude() {
            return latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public Long getLastFix() {
            return lastFix;
        }

        public void setLatitude(final Double latitude) {
            this.latitude = latitude;
        }

        public void setLongitude(final Double longitude) {
            this.longitude = longitude;
        }

        public void setLastFix(final Long lastFix) {
            this.lastFix = lastFix;
        }
    }

    public static class Security {

        @SerializedName("rooted")
        private Boolean rooted;
        @SerializedName("default_sms_handler")
        private Boolean defaultSmsHandler;

        public Boolean getRooted() {
            return rooted;
        }

        public Boolean getDefaultSmsHandler() {
            return defaultSmsHandler;
        }

        public void setRooted(final Boolean rooted) {
            this.rooted = rooted;
        }

        public void setDefaultSmsHandler(final Boolean defaultSmsHandler) {
            this.defaultSmsHandler = defaultSmsHandler;
        }
    }

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("device_info")
    private DeviceInfo deviceInfo;
    @SerializedName("sim_cards")
    private SIMCard[] simCards;
    @SerializedName("wifi")
    private WiFi wifi;
    @SerializedName("networks")
    private Network[] networks;
    @SerializedName("location")
    private Location location;
    @SerializedName("security")
    private Security security;

    public String getRequestId() {
        return requestId;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public SIMCard[] getSimCards() {
        return simCards;
    }

    public WiFi getWifi() {
        return wifi;
    }

    public Network[] getNetworks() {
        return networks;
    }

    public Location getLocation() {
        return location;
    }

    public Security getSecurity() {
        return security;
    }

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public void setDeviceInfo(final DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public void setSimCards(final SIMCard[] simCards) {
        this.simCards = simCards;
    }

    public void setWifi(final WiFi wifi) {
        this.wifi = wifi;
    }

    public void setNetworks(final Network[] networks) {
        this.networks = networks;
    }

    public void setLocation(final Location location) {
        this.location = location;
    }

    public void setSecurity(final Security security) {
        this.security = security;
    }
}
