package com.haud.stu.android;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsMessage;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.haud.stu.android.db.DBHelper;
import com.haud.stu.android.db.DBModel;
import com.haud.stu.android.models.SmsCallbackResponse;
import com.haud.stu.android.models.StatusResponse;
import com.haud.stu.android.utils.GeneralUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class SMSManager {

    private static final String KEY_SHARED_PREF = "com.haud.stu.smsfilters";
    private static final String KEY_SMSFILTER = "SMS_FILTERS";

    private static class SmsMessageWrapper {
        private long id;
        private String uuid;
        private byte[][] pdus;
        private long timestamp;
        private String format;
        private int slot;
        private String imsi;
        private String iccid;

        public SmsMessageWrapper(long id, String uuid, byte[][] pdus, long timestamp, String format,
                                 int slot, String imsi, String iccid) {
            this.id = id;
            this.uuid = uuid;
            this.pdus = pdus;
            this.timestamp = timestamp;
            this.format = format;
            this.slot = slot;
            this.imsi = imsi;
            this.iccid = iccid;
        }

        public String getUuid() {
            return uuid;
        }

        public byte[][] getPdus() {
            return pdus;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public long getId() {
            return id;
        }

        public String getFormat() {
            return format;
        }

        public int getSlot() {
            return slot;
        }

        public String getImsi() {
            return imsi;
        }

        public String getIccid() {
            return imsi;
        }


    }

    public static void insertIntoLocalStorage(String uuid, byte[][] pdus, Context context,
                                              String format, int slot, String imsi, String iccid) {
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(DBModel.SMSEntry.COLUMN_NAME_TIMESTAMP, System.currentTimeMillis());
            values.put(DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID, uuid);
            values.put(DBModel.SMSEntry.COLUMN_NAME_PDU_FORMAT, format);
            values.put(DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT, slot);
            values.put(DBModel.SMSEntry.COLUMN_NAME_SIM_IMSI, imsi);
            values.put(DBModel.SMSEntry.COLUMN_NAME_SIM_ICCID, iccid);

            for (byte[] pdu : pdus) {
                final SmsMessage sms;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    sms = SmsMessage.createFromPdu(pdu, format);
                } else {
                    sms = SmsMessage.createFromPdu(pdu);
                }
                values.put(DBModel.SMSEntry.COLUMN_NAME_ADDRESS, sms.getOriginatingAddress());
                values.put(DBModel.SMSEntry.COLUMN_NAME_BODY, sms.getMessageBody());
            }

            final long parentId = db.insert(DBModel.SMSEntry.TABLE_NAME, "null", values);

            if (parentId < 0) {
                throw new RuntimeException(String.format("UUID: [%s] - Insert of SMS to " +
                        "DB generated row ID [%d].", uuid, parentId));
            }
            for (Object pdu : pdus) {
                ContentValues valuesPdu = new ContentValues();
                valuesPdu.put(DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID, parentId);
                valuesPdu.put(DBModel.SMSEntryPdu.COLUMN_NAME_PDU, (byte[]) pdu);
                long id = db.insert(DBModel.SMSEntryPdu.TABLE_NAME, "null", valuesPdu);
                if (id < 0) {
                    throw new RuntimeException(String.format("UUID: [%s] - Insert of SMS PDU to " +
                            "DB generated row ID [%d].", uuid, id));
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        } finally {
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }

    public static void _removeFromLocalStorage(String uuid, Context context) {
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            db.execSQL(String.format("DELETE FROM `%s` WHERE `%s` IN (SELECT `%s` FROM `%s` WHERE " +
                            "`%s` = '')", DBModel.SMSEntryPdu.TABLE_NAME, DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID,
                    DBModel.SMSEntry._ID, DBModel.SMSEntry.TABLE_NAME, DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID,
                    uuid));
            db.execSQL(String.format("DELETE FROM `%s` WHERE `%s` = '%s'",
                    DBModel.SMSEntry.TABLE_NAME, DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID,
                    uuid));
            db.setTransactionSuccessful();
            db.endTransaction();
        } finally {
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }

    public static void updateFromLocalStorage(String uuid, Context context) {
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            Integer statusValue = 1;
            db.execSQL("UPDATE sms_entry SET status = "+statusValue+" WHERE event_uuid='"+uuid+"' ");
            db.setTransactionSuccessful();
            db.endTransaction();
        } finally {
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }



    public static void insertIntoSmsStore(String uuid, byte[][] pdus, final Context ctx,
                                          final String pduFormat, final int slot) {
        if (GeneralUtils.isDefaultMessagingApp(ctx) != GeneralUtils.UnknownBoolean.TRUE) {
            return; //handled by somebody else
        }
        for (byte[] pdu : pdus) {
            final SmsMessage sms;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sms = SmsMessage.createFromPdu(pdu, pduFormat);
            } else {
                sms = SmsMessage.createFromPdu(pdu);
            }
            ContentValues values = new ContentValues();
            values.put("address", sms.getOriginatingAddress());
            values.put("date", sms.getTimestampMillis());
            values.put("read", 0);
            values.put("status", sms.getStatus());
            values.put("type", 1);
            values.put("body", sms.getMessageBody());
            values.put("seen", 0);
            values.put("slot", slot);
            ctx.getContentResolver().insert(Uri.parse("content://sms/storage"), values);
        }
    }

    public static void updateAllowedResponsesKeywords(StatusResponse resp, Context context) {
        final SharedPreferences sharedPref = context.getSharedPreferences(
                KEY_SHARED_PREF, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        editor.putString(KEY_SMSFILTER, gson.toJson(resp.getSmsMessageStuFilters()));
        editor.commit();
    }

    public static boolean isMessageWanted(final String uuid, final byte[][] pdus,
                                          final String pduFormat, final Context context) {
        final SharedPreferences sharedPref = context.getSharedPreferences(
                KEY_SHARED_PREF, Context.MODE_PRIVATE);
        String jsonList = sharedPref.getString(KEY_SMSFILTER, "{}");
        if (jsonList == null) {
            return true;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Type setStringType = new TypeToken<HashSet<String>>() {
        }.getType();
        Log.d(SMSManager.class.getSimpleName(), String.format("UUID: [%s] SMS Filter JSON from " +
                "shared pref:%n%s", uuid, jsonList));
        Set<String> list = gson.fromJson(jsonList, setStringType);
        if (list == null) {
            return true;
        }
        if (list.isEmpty()) {
            return true; //when nothing specified, we want all
        }
        for (final String listItem : list) {
            for (byte[] pdu : pdus) {
                final SmsMessage smsMessage;
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    smsMessage = SmsMessage.createFromPdu(pdu);
                } else {
                    smsMessage = SmsMessage.createFromPdu(pdu, pduFormat);
                }
                if (smsMessage.getOriginatingAddress() != null &&
                        smsMessage.getOriginatingAddress().contains(listItem)) {
                    return true;
                }
                if (smsMessage.getMessageBody() != null &&
                        smsMessage.getMessageBody().contains(listItem)) {
                    return true;
                }
                if (smsMessage.getPseudoSubject() != null &&
                        smsMessage.getPseudoSubject().contains(listItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void processPendingSMSes(final Context context) {
        System.out.println("Trying to push all sms");
        StatusResponse deviceStatusResponse = ApiRequestManager.submitStatusCallback(context,
                UUID.randomUUID().toString());
        if (deviceStatusResponse == null) {
            return; //we have a comms issue - retry later
        }
        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getReadableDatabase();
            c = db.rawQuery(String.format("SELECT * FROM `%s`", DBModel.SMSEntry.TABLE_NAME), null);
            while (c.moveToNext()) {
                String uuid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID));
                long ts = c.getLong(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_TIMESTAMP));
                String format = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_PDU_FORMAT));
                long id = c.getLong(c.getColumnIndex(DBModel.SMSEntry._ID));
                Cursor cPdus = null;
                List<byte[]> pdus = new ArrayList<>();
                try {
                    cPdus = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` = ?",
                            DBModel.SMSEntryPdu.TABLE_NAME, DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID),
                            new String[]{String.valueOf(id)});
                    while (cPdus.moveToNext()) {
                        pdus.add(cPdus.getBlob(cPdus.getColumnIndex(DBModel.SMSEntryPdu.COLUMN_NAME_PDU)));
                    }
                } finally {
                    if (cPdus != null) {
                        cPdus.close();
                    }
                }
                int slot = c.getInt(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT));
                String imsi = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_IMSI));
                String iccid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_ICCID));

                final SmsMessageWrapper msgWrapper = new SmsMessageWrapper(id, uuid,
                        pdus.toArray(new byte[pdus.size()][]), ts, format, slot, imsi, iccid);

//                if (deviceStatusResponse.isBlockOperation() ||
//                        !isMessageWanted(uuid, msgWrapper.getPdus(), format, context)) {
//                    //SMSManager.removeFromLocalStorage(uuid, context);
//                    return;
//                }

                final SmsCallbackResponse response;
                response = ApiRequestManager.submitSmsCallback(
                        msgWrapper.getUuid(), msgWrapper.getPdus(), msgWrapper.getFormat(),
                        msgWrapper.getTimestamp(), msgWrapper.getSlot(), msgWrapper.getImsi(), msgWrapper.getIccid(),
                        GeneralUtils.getImeiForSlot(slot, context), context);
                if (response != null) {
                    //if null, it will remain in DB, and picked up by a background task and retried later.
                    // otherwise clean up.
                    //SMSManager.removeFromLocalStorage(uuid, context);
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }


    public static void pushPendingSMSes(final Context context) {
        System.out.println("Trying to push all sms");

        StatusResponse deviceStatusResponse = ApiRequestManager.submitStatusCallback(context,
                UUID.randomUUID().toString());

        if (deviceStatusResponse == null) {
            return; //we have a comms issue - retry later
        }

        DBHelper dbHelper = null;
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            dbHelper = new DBHelper(context);
            db = dbHelper.getReadableDatabase();
            String q = "SELECT * FROM sms_entry WHERE status = 0";
            c = db.rawQuery(q, null);

            while (c.moveToNext()) {
                String uuid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_EVENT_UUID));
                long ts = c.getLong(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_TIMESTAMP));
                String format = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_PDU_FORMAT));
                long id = c.getLong(c.getColumnIndex(DBModel.SMSEntry._ID));
                Cursor cPdus = null;
                List<byte[]> pdus = new ArrayList<>();
                try {
                    cPdus = db.rawQuery(String.format("SELECT * FROM `%s` WHERE `%s` = ?",
                            DBModel.SMSEntryPdu.TABLE_NAME, DBModel.SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID),
                            new String[]{String.valueOf(id)});
                    while (cPdus.moveToNext()) {
                        pdus.add(cPdus.getBlob(cPdus.getColumnIndex(DBModel.SMSEntryPdu.COLUMN_NAME_PDU)));
                    }
                } finally {
                    if (cPdus != null) {
                        cPdus.close();
                    }
                }
                int slot = c.getInt(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_SLOT));
                String imsi = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_IMSI));
                String iccid = c.getString(c.getColumnIndex(DBModel.SMSEntry.COLUMN_NAME_SIM_ICCID));

                final SmsMessageWrapper msgWrapper = new SmsMessageWrapper(id, uuid,
                        pdus.toArray(new byte[pdus.size()][]), ts, format, slot, imsi, iccid);

                final SmsCallbackResponse response;

                response = ApiRequestManager.submitSmsCallback(
                        msgWrapper.getUuid(), msgWrapper.getPdus(), msgWrapper.getFormat(),
                        msgWrapper.getTimestamp(), msgWrapper.getSlot(), msgWrapper.getImsi(), msgWrapper.getIccid(),
                        GeneralUtils.getImeiForSlot(slot, context), context);
                if (response != null) {
                    SMSManager.updateFromLocalStorage(uuid, context);
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            if (dbHelper != null) {
                dbHelper.close();
            }
        }
    }


    public static void handleNewPdus(final String eventUuid, final Object[] pdusGeneric,
                                     final String format, final Context ctx,
                                     final int slot) {
        final String uuid = UUID.randomUUID().toString();
        final long timestamp = System.currentTimeMillis();
        final byte[][] pdus = new byte[pdusGeneric.length][];
        for (int i = 0; i < pdusGeneric.length; i++) {
            pdus[i] = (byte[]) pdusGeneric[i];
        }
        final String imsi = GeneralUtils.getImsiForSlot(slot, ctx);
        final String iccid = GeneralUtils.getIccidForSlot(slot, ctx);


        SMSManager.insertIntoLocalStorage(uuid, pdus, ctx, format, slot, imsi, iccid);
        StatusResponse statusResp = ApiRequestManager.submitStatusCallback(ctx, uuid);
        if (!isMessageWanted(uuid, pdus, format, ctx)) {
            SMSManager.insertIntoSmsStore(uuid, pdus, ctx, format, slot);
            return;
        }
//        if (statusResp.isBlockOperation()) {
//            //SMSManager.removeFromLocalStorage(uuid, ctx);
//            return;
//        }
        final SmsCallbackResponse response;
        response = ApiRequestManager.submitSmsCallback(uuid, pdus, format,
                timestamp, slot, imsi, iccid, GeneralUtils.getImeiForSlot(slot, ctx), ctx);

        System.out.println("submitSmsCallback response " + response);

        if (response != null) {
            //if null, it will remain in DB, and picked up by a background task and retried later.
            // otherwise clean up.
            //SMSManager.removeFromLocalStorage(uuid, ctx);
            System.out.println("updating updateFromLocalStorage " + uuid);

            SMSManager.updateFromLocalStorage(uuid, ctx);
        }
    }

}
