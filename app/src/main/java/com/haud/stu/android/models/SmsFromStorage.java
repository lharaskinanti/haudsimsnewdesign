package com.haud.stu.android.models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.annotations.SerializedName;
import com.haud.stu.android.ApiRequestManager;
import com.haud.stu.android.db.DBHelper;
import com.haud.stu.android.db.DBModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;


/**
 * Created by Al Kasih on 29/07/2020.
 */

public class SmsFromStorage {

    //public String address;
    @SerializedName("timestamp")
    private long timestamp;
    @SerializedName("event_uuid")
    private String event_uuid;
    @SerializedName("format")
    private String pdu_format;
    @SerializedName("slot")
    private String sim_slot;

    @SerializedName("imsi")
    private String sim_imsi;
    @SerializedName("address")
    public String address;
    @SerializedName("body")
    private String body;
    @SerializedName("status")
    private Integer status;


    @SerializedName("entry_id")
    private long sms_entry_id;
    @SerializedName("pdu")
    private List<Integer> pdu;

    public void setTimestamp(long timestamp) { this.timestamp = timestamp; }
    public void setEvent_uuid(String event_uuid) { this.event_uuid = event_uuid; }
    public void setPdu_format(String pdu_format) { this.pdu_format = pdu_format; }
    public void setSim_slot(String sim_slot) { this.sim_slot = sim_slot; }
    public void setSim_imsi(String sim_imsi) { this.sim_imsi = sim_imsi; }
    public void setSms_entry_id(long sms_entry_id) { this.sms_entry_id = sms_entry_id; }

    public void setAddress(String address) { this.address = address; }
    public void setBody(String body) { this.body = body; }

    public void setStatus (Integer status) { this.status = status; }
    public void setPdu (List<Integer> pdu) { this.pdu = pdu; }

    public long getTimestamp() { return timestamp; }
    public String getEvent_uuid() { return event_uuid; }
    public String getPdu_format() { return pdu_format; }
    public String getSim_slot() { return sim_slot; }
    public String getSim_imsi() { return sim_imsi; }
    public String getAddress() { return address; }
    public String getBody() { return body; }
    public long getSms_entry_id() { return sms_entry_id; }
    public List<Integer> getPdu() { return pdu; }
    public Integer getStatus() { return status; }


//    public SmsFromStorage(long timestamp, String event_uuid, String pdu_format,
//                          String sim_slot, String sim_imsi, long sms_entry_id, List<Integer> pdu) {
//        timestamp = timestamp;
//        event_uuid = event_uuid;
//        pdu_format = pdu_format;
//        sim_slot = sim_slot;
//        sim_imsi = sim_imsi;
//        sms_entry_id = sms_entry_id;
//        pdu = pdu;
//    }

    @Override
    public String toString() {
        return "timestamp: " + this.getTimestamp() +
                ", event_uuid: " + this.getEvent_uuid()  +
                ", pdu_format: " + this.getPdu_format() +
                ", sim_slot: " + this.getSim_slot() +
                ", sim_imsi: " + this.getSim_imsi() +
                ", address: " + this.getAddress() +
                ", body: " + this.getBody() +
                ", sms_entry_id: " + this.getSms_entry_id() +
                ", pdu: " + this.getPdu();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SmsFromStorage sms = (SmsFromStorage) o;
        return address == sms.address ;

    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }


}
