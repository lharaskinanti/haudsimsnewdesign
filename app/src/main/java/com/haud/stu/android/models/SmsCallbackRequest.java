package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class SmsCallbackRequest {

    public static class Pdu {

        @SerializedName("seq")
        private Integer seq;
        @SerializedName("pdu")
        private String pdu;
        @SerializedName("sms_body")
        private String smsBody;
        @SerializedName("sms_message_class")
        private String smsMessageClass;
        @SerializedName("sms_originating_address")
        private String smsOriginatingAddress;
        @SerializedName("sms_pid")
        private Integer smsPid;
        @SerializedName("sms_pseudo_subject")
        private String smsPseudoSubject;
        @SerializedName("sms_smsc")
        private String smsSmsc;
        @SerializedName("sms_status")
        private Integer smsStatus;
        @SerializedName("sms_smsc_timestamp")
        private Long smsSmscTimestamp;
        @SerializedName("sms_userdata")
        private String smsUserdata;

        public Integer getSeq() {
            return seq;
        }

        public String getPdu() {
            return pdu;
        }

        public void setSeq(final Integer seq) {
            this.seq = seq;
        }

        public void setPdu(final String pdu) {
            this.pdu = pdu;
        }

        public String getSmsBody() {
            return smsBody;
        }

        public void setSmsBody(final String smsBody) {
            this.smsBody = smsBody;
        }

        public String getSmsMessageClass() {
            return smsMessageClass;
        }

        public void setSmsMessageClass(final String smsMessageClass) {
            this.smsMessageClass = smsMessageClass;
        }

        public String getSmsOriginatingAddress() {
            return smsOriginatingAddress;
        }

        public void setSmsOriginatingAddress(final String smsOriginatingAddress) {
            this.smsOriginatingAddress = smsOriginatingAddress;
        }

        public Integer getSmsPid() {
            return smsPid;
        }

        public void setSmsPid(final Integer smsPid) {
            this.smsPid = smsPid;
        }

        public String getSmsPseudoSubject() {
            return smsPseudoSubject;
        }

        public void setSmsPseudoSubject(final String smsPseudoSubject) {
            this.smsPseudoSubject = smsPseudoSubject;
        }

        public String getSmsSmsc() {
            return smsSmsc;
        }

        public void setSmsSmsc(final String smsSmsc) {
            this.smsSmsc = smsSmsc;
        }

        public Integer getSmsStatus() {
            return smsStatus;
        }

        public void setSmsStatus(final Integer smsStatus) {
            this.smsStatus = smsStatus;
        }

        public Long getSmsSmscTimestamp() {
            return smsSmscTimestamp;
        }

        public void setSmsSmscTimestamp(final Long smsSmscTimestamp) {
            this.smsSmscTimestamp = smsSmscTimestamp;
        }

        public String getSmsUserdata() {
            return smsUserdata;
        }

        public void setSmsUserdata(final String smsUserdata) {
            this.smsUserdata = smsUserdata;
        }
    }

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("device_serial")
    private String deviceSerial;
    @SerializedName("generated_on")
    private Long generatedOn;
    @SerializedName("pdus")
    private Pdu[] pdus;
    @SerializedName("pdu_format")
    private String pduFormat;
    @SerializedName("sim_slot")
    private Integer simSlot;

    @SerializedName("sim_imsi")
    private String simImsi;

    @SerializedName("sim_iccid")
    private String simIccid;

    @SerializedName("imei")
    private String imei;

    public String getRequestId() {
        return requestId;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public void setDeviceSerial(final String deviceSerial) {
        this.deviceSerial = deviceSerial;
    }

    public Long getGeneratedOn() {
        return generatedOn;
    }

    public Pdu[] getPdus() {
        return pdus;
    }

    public void setSimIccid(String iccid) { this.simIccid = iccid;}

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public void setGeneratedOn(final Long generatedOn) {
        this.generatedOn = generatedOn;
    }

    public void setPdus(final Pdu[] pdus) {
        this.pdus = pdus;
    }

    public String getPduFormat() {
        return pduFormat;
    }

    public void setPduFormat(final String pduFormat) {
        this.pduFormat = pduFormat;
    }

    public Integer getSimSlot() {
        return simSlot;
    }

    public void setSimSlot(Integer simSlot) {
        this.simSlot = simSlot;
    }

    public String getSimImsi() {
        return simImsi;
    }

    public String getSimIccid() {
        return simIccid;
    }

    public void setSimImsi(String simImsi) {
        this.simImsi = simImsi;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
