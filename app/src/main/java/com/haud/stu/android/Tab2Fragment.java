package com.haud.stu.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.haud.stu.android.Activities.DashboardActivity;
import com.haud.stu.android.models.SmsFromStorage;
import com.haud.stu.android.utils.SmslistsUtils;

import java.util.ArrayList;
import java.util.List;

public class Tab2Fragment extends Fragment {

    private View rootView;
    private Button toTryActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        final int slot = 0;
        getSmsfromSlot(slot);
        toTryActivity = (Button) rootView.findViewById(R.id.tryActivity);

        toTryActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toTryActivity = new Intent(getActivity(), DashboardActivity.class);
                startActivity(toTryActivity);
            }
        });

        return rootView;
    }


    private void getSmsfromSlot(final int slot) {
        final Context ctx = getActivity();
        System.out.println("Tab two calling sms");
        final List<SmsFromStorage> smsall = SmslistsUtils.listStorageSMSes(ctx, slot);
        System.out.println(smsall);

        ArrayAdapter<SmsFromStorage> adapter = new StorageSmsAdapter(ctx, 0, (ArrayList<SmsFromStorage>) smsall);
        ListView listView = (ListView) rootView.findViewById(R.id.customListView);
        listView.setAdapter(adapter);

        AdapterView.OnItemClickListener adapterViewListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SmsFromStorage sms = smsall.get(position);
                Intent intent = new Intent(getActivity(), SmsFromOa.class);
                intent.putExtra("from", sms.getAddress());
                intent.putExtra("slot", slot);
                startActivityForResult(intent, 1000);
            }
        };

        listView.setOnItemClickListener(adapterViewListener);
    }

}


//class storageArrayAdapter extends ArrayAdapter<SmsFromStorage> {
//    private Context context;
//
//    private static class ViewHolder {
//        private TextView itemView;
//    }
//
//    public storageArrayAdapter(Context context, int resource, ArrayList<SmsFromStorage> objects) {
//        super(context, resource, objects);
//    }
//
//
//    public View getView(int position, View convertView, ViewGroup parent) {
//        SmsFromStorage inboxList = getItem(position);
//        LayoutInflater inflater = LayoutInflater.from(this.getContext());
//
//        View view;
//        view = inflater.inflate(R.layout.storage, null);
//
//        TextView description = (TextView) view.findViewById(R.id.description);
//        TextView address = (TextView) view.findViewById(R.id.address);
//        ImageView image = (ImageView) view.findViewById(R.id.image);
//
//        String completeAddress = inboxList.getAddress();
//        address.setText(completeAddress);
//        int descriptionLength = inboxList.getBody().length();
//        if(descriptionLength >= 100){
//            String descriptionTrim = inboxList.getBody().substring(0, 100) + "...";
//            description.setText(descriptionTrim);
//        }else{
//            description.setText(inboxList.getBody());
//        }
//
//        return view;
//    }
//
//}
