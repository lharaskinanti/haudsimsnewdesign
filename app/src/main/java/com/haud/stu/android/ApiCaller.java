package com.haud.stu.android;

import android.util.Base64;
import android.util.Log;

import com.haud.stu.android.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.haud.stu.android.models.BaseEncryptedRequest;
import com.haud.stu.android.models.BaseEncryptedResponse;
import com.haud.stu.android.models.HandshakeResponse;
import com.haud.stu.android.models.SmsCallbackRequest;
import com.haud.stu.android.models.SmsCallbackResponse;
import com.haud.stu.android.models.StatusRequest;
import com.haud.stu.android.models.StatusResponse;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutionException;

public class ApiCaller {

    public static class BadApiResponseException extends Exception {
        public BadApiResponseException() {
        }

        public BadApiResponseException(final String errMsg) {
            super(errMsg);
        }

        public BadApiResponseException(final Throwable tr) {
            super(tr);
        }

        public BadApiResponseException(final String errMsg, final Throwable tr) {
            super(errMsg, tr);
        }
    }

    private final STUSecurityManager stuSecurityManager;
    private final byte[] sessionSymmetricKey;

    private ApiCaller() throws CertificateException, IOException,
            InterruptedException, ExecutionException, NoSuchAlgorithmException, JSONException,
            SignatureException, NoSuchProviderException, InvalidKeyException, BadApiResponseException {
        stuSecurityManager = STUSecurityManager.newInstance();
        sessionSymmetricKey = stuSecurityManager.generateRandomSymmetricKey(
                BuildConfig.SYMMETRIC_KEY_SIZE);
    }

    public static ApiCaller newInstance() throws CertificateException, IOException,
            InterruptedException, ExecutionException, NoSuchAlgorithmException, JSONException,
            SignatureException, NoSuchProviderException, InvalidKeyException, BadApiResponseException {
        return new ApiCaller();
    }

    public static String getLatestApiPubCert() throws IOException, JSONException, BadApiResponseException {
        final String url = BuildConfig.API_URL_GET_LATEST_CERT;
        final byte[] responseRaw = doApiCall(url, "GET", null);
        final String responseJson = new String(responseRaw, "UTF-8");
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        final HandshakeResponse response = gson.fromJson(responseJson, HandshakeResponse.class);
        if (response == null) {
            return null;
        }
        return response.getPublicKey();
    }

    public static byte[] doApiCall(final String urlStr, final String verb, final byte[] body)
            throws IOException, JSONException, BadApiResponseException {
        URL url = new URL(urlStr);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(verb);
        urlConnection.setReadTimeout(BuildConfig.API_HTTP_READ_TIMEOUT);
        urlConnection.setConnectTimeout(BuildConfig.API_HTTP_CONNECT_TIMEOUT);
        if(body != null) {
            urlConnection.setDoOutput(true);
        }
        if (body != null) {
            OutputStream os = null;
            try {
                os = urlConnection.getOutputStream();
                for (int i = 0; i < body.length; i++) {
                    os.write(body[i]);
                }
            } finally {
                if (os != null) {
                    os.close();
                }
            }
        }
        urlConnection.connect();
        final int statusCode = urlConnection.getResponseCode();
        ;
        if (statusCode < 200 || statusCode > 299) {
            Log.e(ApiCaller.class.getSimpleName(), String.format("Handshake returned response code " +
                    "[%d].", statusCode));
            throw new BadApiResponseException(String.format("Handshake returned response code " +
                    "[%d].", statusCode));
        }
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        try {
            is = urlConnection.getInputStream();
            baos = new ByteArrayOutputStream();
            byte[] buf = new byte[8192];
            int rc;
            while ((rc = is.read(buf)) != -1) {
                baos.write(buf, 0, rc);
            }
            return baos.toByteArray();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (final IOException ex) {
                    //do nothing
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (final IOException ex) {
                    //do nothing
                }
            }
        }
    }

    private BaseEncryptedRequest buildEncryptedRequest(final Object request) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        final String json = gson.toJson(request);
        final byte[] reqBody;
        try {
            reqBody = json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(ApiCaller.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
        final byte[] encSymKey = stuSecurityManager.encryptViaPubCert(sessionSymmetricKey);
        final byte[] encBody = stuSecurityManager.encryptViaSymmetricKey(reqBody, sessionSymmetricKey);
        final BaseEncryptedRequest encRequest = new BaseEncryptedRequest();
        encRequest.setData(Base64.encodeToString(encBody, Base64.NO_WRAP));
        encRequest.setSymmetricKey(Base64.encodeToString(encSymKey, Base64.NO_WRAP));
        return encRequest;
    }

    private BaseEncryptedResponse doEncryptedRestPost(final Object request, final String url)
            throws IOException, JSONException, BadApiResponseException {
        final BaseEncryptedRequest encReq = buildEncryptedRequest(request);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        final String json = gson.toJson(encReq);
        final byte[] rawReq;
        try {
            rawReq = json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        final byte[] rawResp = doApiCall(url, "POST", rawReq);
        if (rawResp == null || rawResp.length == 0) {
            return null;
        }
        final String resp = new String(rawResp, "UTF-8");
        return gson.fromJson(resp, BaseEncryptedResponse.class);
    }

    private String extractFromEncryptedResponse(final BaseEncryptedResponse response) {
        if (response == null) {
            Log.i(ApiCaller.class.getSimpleName(), "Null response.");
            return null;
        }
        if (response.getResponseId() == null) {
            Log.i(ApiCaller.class.getSimpleName(), "Null response ID - skipping.");
            return null;
        }
        if (response.getData() == null) {
            Log.i(ApiCaller.class.getSimpleName(), "Null response data - skipping.");
            return null;
        }
        final byte[] encData = Base64.decode(response.getData(), Base64.NO_WRAP);
        final byte[] data = stuSecurityManager.decryptViaSymmetricKey(encData, sessionSymmetricKey);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(ApiCaller.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public StatusResponse postStatus(final StatusRequest statusRequest)
            throws IOException, JSONException, BadApiResponseException {
        final String url = BuildConfig.API_URL_POST_STATUS;
        Log.d(ApiCaller.class.getSimpleName(), String.format("Calling URL: %s", url));
        final BaseEncryptedResponse encResp = doEncryptedRestPost(statusRequest, url);
        if (encResp == null) {
            return null;
        }
        final String responseJson = extractFromEncryptedResponse(encResp);
        if (responseJson == null) {
            return null;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.fromJson(responseJson, StatusResponse.class);
    }

    public SmsCallbackResponse postSmsCallback(final SmsCallbackRequest smsCallbackRequest)
            throws IOException, JSONException, BadApiResponseException {
        final String url = BuildConfig.API_URL_POST_SMSCALLBACK;
        Log.d(ApiCaller.class.getSimpleName(), String.format("Calling URL: %s", url));
        final BaseEncryptedResponse encResp = doEncryptedRestPost(smsCallbackRequest, url);
        if (encResp == null) {
            return null;
        }
        final String responseJson = extractFromEncryptedResponse(encResp);
        if (responseJson == null) {
            return null;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.fromJson(responseJson, SmsCallbackResponse.class);
    }

}
