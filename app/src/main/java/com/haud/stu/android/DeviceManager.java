package com.haud.stu.android;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;

import com.haud.stu.android.utils.GeneralUtils;

public class DeviceManager {
    private static boolean active = false;

    public static boolean isActive() {
        return active;
    }

    public static void setActive(final boolean active) {
        DeviceManager.active = active;
    }

    public static void doPrechecks(final Context ctx){
        checkDefaultMessagingApp(ctx);
    }

    private static void checkDefaultMessagingApp(Context ctx) {
        if(GeneralUtils.isDefaultMessagingApp(ctx) != GeneralUtils.UnknownBoolean.FALSE){
            return;
        }
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
            return;
        }
        Intent sendIntent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        sendIntent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, ctx.getPackageName());
        ctx.startActivity(sendIntent);
    }
}
