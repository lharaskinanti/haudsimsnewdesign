package com.haud.stu.android;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.util.Base64;
import android.util.Log;

import com.haud.stu.android.listeners.LocationListener;
import com.haud.stu.android.models.SmsCallbackRequest;
import com.haud.stu.android.models.SmsCallbackResponse;
import com.haud.stu.android.models.StatusRequest;
import com.haud.stu.android.models.StatusResponse;
import com.haud.stu.android.utils.GeneralUtils;

import java.util.ArrayList;
import java.util.List;

public class ApiRequestManager {
    public static StatusRequest generateStatusRequest(final Context context, final String uuid) {
        Log.i(ApiRequestManager.class.getSimpleName(), String.format("[%s] - Generating new " +
                "status request.", uuid));
        final StatusRequest request = new StatusRequest();
        request.setRequestId(uuid);
        final StatusRequest.DeviceInfo deviceInfo = new StatusRequest.DeviceInfo();
        deviceInfo.setAppVersion(GeneralUtils.getAppVersion(context));
        deviceInfo.setAppVersionStr(GeneralUtils.getAppVersionStr(context));



        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("Android","Android ID : "+android_id);

        String android_serial = Build.SERIAL == null ? Build.SERIAL : android_id;

        //deviceInfo.setSerialNumber(Build.SERIAL);
        deviceInfo.setSerialNumber(android_serial);


        deviceInfo.setOs("Android");
        deviceInfo.setOsVersion(Build.VERSION.RELEASE);
        deviceInfo.setOsApiLevel((double) Build.VERSION.SDK_INT);
        deviceInfo.setBatteryLevel(GeneralUtils.getBatteryLevel(context));
        deviceInfo.setPowerMode(GeneralUtils.isCharging(context) ? StatusRequest.PowerMode.AC
                : StatusRequest.PowerMode.BATTERY);
        deviceInfo.setModel(String.format("%s %s", Build.MANUFACTURER, Build.MODEL));
        request.setDeviceInfo(deviceInfo);
        final StatusRequest.Security security = new StatusRequest.Security();
        security.setRooted(GeneralUtils.isDeviceRooted());
        security.setDefaultSmsHandler(GeneralUtils.isDefaultMessagingApp(context)
                == GeneralUtils.UnknownBoolean.TRUE ? true : false);
        request.setSecurity(security);
        final List<GeneralUtils.SimSubscriberInfo> simInfos =
                GeneralUtils.getSimSubscriptionInfos(context);
        final List<StatusRequest.SIMCard> simcards = new ArrayList<>();
        for (GeneralUtils.SimSubscriberInfo simInfo : simInfos) {
            StatusRequest.SIMCard simCard = new StatusRequest.SIMCard();
            simCard.setSlot(simInfo.getSlot());
            simCard.setImei(simInfo.getImei());
            simCard.setImsi(simInfo.getImsi());
            simCard.setIccid(simInfo.getIccId());
            simCard.setMcc(simInfo.getMcc());
            //final int mnc = simInfo.getMnc();
            //final String tmnc = String.format("%02d", mnc);
            simCard.setMnc(simInfo.getMnc());
            //simCard.setMnc(tmnc);

            simCard.setCarrier(simInfo.getSimCarrierName());
            simCard.setNumber(simInfo.getPhoneNumber());
            simCard.setRoaming(simInfo.isRoaming());
            simcards.add(simCard);
        }
        request.setSimCards(simcards.toArray(new StatusRequest.SIMCard[simcards.size()]));
        StatusRequest.WiFi wifi = new StatusRequest.WiFi();
        wifi.setEnabled(GeneralUtils.isWifiEnabled(context));
        if (wifi.getEnabled()) {
            wifi.setSignalStrength((double) GeneralUtils.getWifiSignalStrength(context));
            wifi.setSsid(GeneralUtils.getWiFiSsid(context));
        }
        request.setWifi(wifi);
        request.setNetworks(GeneralUtils.getNetworkInfo(context));
        if (LocationListener.isLocationServiceEnabled(context)) {
            Log.d(ApiRequestManager.class.getSimpleName(), "Location service is enabled.");
            Location latestLocation = LocationListener.getLatestLocation(context);
            if (latestLocation != null) {
                Log.d(ApiRequestManager.class.getSimpleName(), "Location object found.");
                StatusRequest.Location location = new StatusRequest.Location();
                location.setLatitude(latestLocation.getLatitude());
                location.setLongitude(latestLocation.getLongitude());
                location.setLastFix(latestLocation.getTime());
                request.setLocation(location);
            } else {
                Log.d(ApiRequestManager.class.getSimpleName(), "Location object not found.");
            }
        } else {
            Log.d(ApiRequestManager.class.getSimpleName(), "Location service is disabled.");
        }
        return request;
    }

    public static StatusResponse submitStatusCallback(final Context context, final String uuid) {
        try {
            StatusResponse resp = submitStatusCallback(context, ApiCaller.newInstance(), uuid);
            if (resp != null) {
                SMSManager.updateAllowedResponsesKeywords(resp, context);
            }
            return resp;
        } catch (final Exception e) {
            Log.e(ApiRequestManager.class.getSimpleName(), uuid, e);
            //todo: proper error handling
            return null;
        }
    }

    public static StatusResponse submitStatusCallback(final Context context,
                                                      final ApiCaller apiCaller,
                                                      final String uuid) {
        try {
            return apiCaller.postStatus(generateStatusRequest(context, uuid));
        } catch (final Exception e) {
            Log.e(ApiRequestManager.class.getSimpleName(), uuid, e);
            //todo: proper error handling
            return null;
        }
    }

    public static SmsCallbackRequest generateSmsCallbackRequest(final String uuid,
                                                                final byte[][] pdus,
                                                                final String pduFormat,
                                                                final long eventTimestamp,
                                                                final int slot,
                                                                final String imsi,
                                                                final String iccid,
                                                                final String imei, final Context context) {
        Log.i(ApiRequestManager.class.getSimpleName(), String.format("[%s] - Generating new " +
                "SMS callback request.", uuid));
        final SmsCallbackRequest request = new SmsCallbackRequest();
        request.setRequestId(uuid);

        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("Android","Android ID : "+android_id);

        String android_serial = Build.SERIAL == "unknown" ? android_id : Build.SERIAL;

        //request.setDeviceSerial(Build.SERIAL);
        request.setDeviceSerial(android_serial);
        request.setGeneratedOn(eventTimestamp);
        request.setSimSlot(slot);
        request.setSimImsi(imsi);
        request.setSimIccid(iccid);
        request.setImei(imei);
        final SmsCallbackRequest.Pdu[] pdusReq = new SmsCallbackRequest.Pdu[pdus.length];
        for (int i = 0; i < pdus.length; i++) {
            final byte[] pduRaw = pdus[i];
            final SmsCallbackRequest.Pdu pdu = new SmsCallbackRequest.Pdu();
            pdu.setSeq(i);
            pdu.setPdu(Base64.encodeToString(pduRaw, Base64.NO_WRAP));
            pdusReq[i] = pdu;
            final SmsMessage sms;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                sms = SmsMessage.createFromPdu(pduRaw);
            } else {
                sms = SmsMessage.createFromPdu(pduRaw, pduFormat);
            }
            if (sms != null) {
                String message = sms.getMessageBody();

                Log.i(ApiRequestManager.class.getSimpleName(), String.format("[%s] - Generating new " +
                        "Message Body is .", message));

                pdu.setSmsBody(sms.getMessageBody());
                SmsMessage.MessageClass msgClass = sms.getMessageClass();
                pdu.setSmsMessageClass(msgClass != null ? msgClass.toString() : null);
                pdu.setSmsOriginatingAddress(sms.getOriginatingAddress());
                pdu.setSmsPid(sms.getProtocolIdentifier());
                pdu.setSmsPseudoSubject(sms.getPseudoSubject());
                pdu.setSmsSmsc(sms.getServiceCenterAddress());
                pdu.setSmsStatus(sms.getStatus());
                pdu.setSmsSmscTimestamp(sms.getTimestampMillis());
                final byte[] userData = sms.getUserData();
                if (userData != null) {
                    pdu.setSmsUserdata(Base64.encodeToString(userData, Base64.NO_WRAP));
                }
            }
        }
        request.setPdus(pdusReq);
        request.setPduFormat(pduFormat);
        return request;
    }

    public static SmsCallbackResponse submitSmsCallback(final String uuid,
                                                        final byte[][] pdus,
                                                        final String pduFormat,
                                                        final long eventTimestamp,
                                                        final int slot,
                                                        final String imsi,
                                                        final String iccid,
                                                        final String imei, final Context context) {
        try {
            return submitSmsCallback(ApiCaller.newInstance(), uuid, pdus, pduFormat, eventTimestamp,
                    slot, imsi, iccid, imei, context);
        } catch (final Exception e) {
            Log.e(ApiRequestManager.class.getSimpleName(), uuid, e);
            //todo: proper error handling
            return null;
        }
    }

    public static SmsCallbackResponse submitSmsCallback(final ApiCaller apiCaller,
                                                        final String uuid,
                                                        final byte[][] pdus,
                                                        final String pduFormat,
                                                        final long eventTimestamp,
                                                        final int slot,
                                                        final String imsi,
                                                        final String iccid,
                                                        final String imei, final Context context) {
        try {
            //you dont do it like this, you should generate first and post the callback then
            return apiCaller.postSmsCallback(generateSmsCallbackRequest(uuid, pdus, pduFormat,
                    eventTimestamp, slot, imsi, iccid, imei, context));
        } catch (final Exception e) {
            Log.e(ApiRequestManager.class.getSimpleName(), uuid, e);
            //todo: proper error handling
            return null;
        }
    }
}
