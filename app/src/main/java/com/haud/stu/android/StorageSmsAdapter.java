package com.haud.stu.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haud.stu.android.models.SmsFromStorage;

import java.util.ArrayList;

public class StorageSmsAdapter extends ArrayAdapter<SmsFromStorage> {

    private Context context;

    private static class ViewHolder {
        private TextView itemView;
    }

    public StorageSmsAdapter(Context context, int resource, ArrayList<SmsFromStorage> objects) {
        super(context, resource, objects);
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        SmsFromStorage inboxList = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(this.getContext());

        View view;
        view = inflater.inflate(R.layout.storage, null);

        TextView description = (TextView) view.findViewById(R.id.description);
        TextView address = (TextView) view.findViewById(R.id.address);
        ImageView image = (ImageView) view.findViewById(R.id.image);

        String completeAddress = inboxList.getAddress();
        address.setText(completeAddress);
        int descriptionLength = inboxList.getBody().length();
        if (descriptionLength >= 100) {
            String descriptionTrim = inboxList.getBody().substring(0, 100) + "...";
            description.setText(descriptionTrim);
        } else {
            description.setText(inboxList.getBody());
        }

        return view;
    }


}
