package com.haud.stu.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.os.Bundle;

import com.haud.stu.android.SMSManager;
import com.haud.stu.android.utils.GeneralUtils;

import java.util.UUID;

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, final Intent intent) {
        final String eventUuid = UUID.randomUUID().toString();
        final String action = intent.getAction();

        Log.i(SmsReceiver.class.getSimpleName(), String.format("UUID: [%s] - New Broadcast Received" +
                " with action [%s].", eventUuid, action));

        if (GeneralUtils.isDefaultMessagingApp(context) == GeneralUtils.UnknownBoolean.TRUE &&
                "android.provider.Telephony.SMS_RECEIVED".equals(action)) {
            return;
        }
        final PendingResult result = goAsync();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final Object[] pdus = (Object[]) intent.getExtras().get("pdus");
                final String format = (String) intent.getExtras().get("format");
                System.out.println(intent.getExtras());
                Bundle bundle;

                final int slot;
                if ((bundle = intent.getExtras()) != null) {
                    //slot = bundle.getInt("slot", -1);
                    int checkSlot = capturedSimSlot(bundle);
                    Log.d("SIM_SLOT"," Slot Number "+capturedSimSlot(bundle));

                    if(checkSlot == 2) {
                        Log.d("SIM_SLOT"," Slot Number 1 From 0");
                        slot = 1;
                    } else {
                        Log.d("SIM_SLOT"," Slot Number 0 From 0");
                        slot = 0;
                    }
                } else {
                    //if it's unavailable, assume a single-sim device i.e. slot 0
                    slot = 0;
                    System.out.println("Slot still not found");
                }

//                if (intent.getExtras().containsKey("slot")) {
//                    slot = (int) intent.getExtras().get("slot");
//                } else {
//                    //if it's unavailable, assume a single-sim device i.e. slot 0
//                    slot = 0;
//                }

                Log.i(SmsReceiver.class.getSimpleName(), String.format("UUID: [%s] - SMS has [%s] PDUs, and" +
                        " format [%s].", eventUuid, pdus == null ? null : pdus.length, format));

                for (int i = 0; i < pdus.length; i++) {
                    final byte[] pdu = (byte[]) pdus[i];
                    Log.d(SmsReceiver.class.getSimpleName(), String.format("PDU [%d / %d]: %s", (i + 1),
                            pdus.length, GeneralUtils.bytesToHex(pdu)));
                }

                SMSManager.handleNewPdus(eventUuid, pdus, format, context, slot);
                Log.i(SmsReceiver.class.getSimpleName(), "Handled new SMS OK.");
                result.finish();
            }
        });
    }

    public int capturedSimSlot(Bundle bundle){

        int whichSIM =-1;
        if (bundle.containsKey("subscription")) {
            whichSIM = bundle.getInt("subscription");
            Log.d("capturedSimSlot"," Slot Number subscription");
        }
        if(whichSIM >=0 && whichSIM < 5){
            /*In some device Subscription id is return as subscriber id*/
            whichSIM = bundle.getInt("subscription");
            Log.d("capturedSimSlot"," Slot Number < 5");
        }else{
            Log.d("capturedSimSlot"," Slot Number > 5");
            if (bundle.containsKey("simId")) {
                whichSIM = bundle.getInt("simId");
                Log.d("capturedSimSlot"," Slot Number simId");
            }else if (bundle.containsKey("com.android.phone.extra.slot")) {
                whichSIM = bundle.getInt("com.android.phone.extra.slot");
                Log.d("capturedSimSlot"," Slot Number com.android");
            }else{
                String keyName = "";
                for(String key : bundle.keySet()){
                    Log.d("capturedSimSlot"," keyname" +key);
                    if(key.contains("com.android.phone.extra"))
                        keyName =key;
                }
                if (bundle.containsKey(keyName)) {
                    whichSIM = bundle.getInt(keyName);
                }
            }
        }
        return whichSIM;
    }
//}
}