package com.haud.stu.android;

import android.util.Log;

import com.haud.stu.android.BuildConfig;

import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutionException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class STUSecurityManager {
    private static final byte[] IV_EMPTY = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static final String CIPHER_STR = "RSA/ECB/PKCS1PADDING";

    private final CertificateFactory certificateFactory;
    private final X509Certificate simBoxApiRootPubCert;
    private X509Certificate simBoxApiPubCert = null;

    private STUSecurityManager() throws CertificateException, IOException {
        this.certificateFactory = getCertFactory();
        this.simBoxApiRootPubCert = getRootCert(this.certificateFactory);
        validate();
    }

    private void validate() throws CertificateNotYetValidException, CertificateExpiredException {
        this.simBoxApiRootPubCert.checkValidity();
    }

    private static CertificateFactory getCertFactory() throws CertificateException {
        return CertificateFactory.getInstance("X509");
    }

    private static X509Certificate parseCertFrom(final CertificateFactory certificateFactory,
                                                 final String certificate)
            throws IOException, CertificateException {
        ByteArrayInputStream in = null;
        X509Certificate cert = null;
        try {
            in = new ByteArrayInputStream(certificate.getBytes("UTF-8"));
            cert = (X509Certificate) certificateFactory.generateCertificate(in);
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return cert;
    }

    private static X509Certificate getRootCert(final CertificateFactory certificateFactory)
            throws IOException, CertificateException {
        return parseCertFrom(certificateFactory, BuildConfig.STU_API_ROOT_PUB_KEY);
    }

    public static STUSecurityManager newInstance() throws CertificateException, IOException,
            InterruptedException, ExecutionException, NoSuchAlgorithmException, JSONException,
            SignatureException, NoSuchProviderException, InvalidKeyException, ApiCaller.BadApiResponseException {
        final STUSecurityManager mgr = new STUSecurityManager();
        mgr.refreshPubCert();
        return mgr;
    }

    public X509Certificate getApiRootCertificate() {
        return getApiRootCertificate();
    }

    public X509Certificate getApiCertificate() throws InterruptedException,
            ExecutionException, CertificateException, JSONException, IOException,
            NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            SignatureException, ApiCaller.BadApiResponseException {
        if (this.simBoxApiPubCert == null) {
            refreshPubCert();
        }
        try {
            this.simBoxApiPubCert.checkValidity();
        } catch (CertificateExpiredException | CertificateNotYetValidException e) {
            //current cached cert has expired - get a new one
            refreshPubCert();
        }
        return this.simBoxApiPubCert;
    }

    public void refreshPubCert() throws InterruptedException,
            ExecutionException, JSONException, IOException, CertificateException,
            NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException,
            SignatureException, ApiCaller.BadApiResponseException {
        final String pubCertStr = ApiCaller.getLatestApiPubCert();
        final X509Certificate pubCert = parseCertFrom(this.certificateFactory, pubCertStr);
        pubCert.checkValidity();
        pubCert.verify(this.simBoxApiRootPubCert.getPublicKey());
        this.simBoxApiPubCert = pubCert;
    }

    public byte[] generateRandomSymmetricKey(final int sz) {
        if(sz % 8 != 0){
            throw new IllegalArgumentException(String.format("Key Size [%d] is not divisible by 8.",
                    sz));
        }
        final SecureRandom random = new SecureRandom();
        final byte[] key = new byte[sz/8];
        random.nextBytes(key);
        return key;
    }

    public byte[] encryptViaSymmetricKey(final byte[] input, final byte[] key) {
        try {
            final Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            c.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(IV_EMPTY));
            return c.doFinal(input);
        } catch (final NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            Log.e(STUSecurityManager.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public byte[] decryptViaSymmetricKey(final byte[] input, final byte[] key) {
        try {
            final Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            c.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(IV_EMPTY));
            return c.doFinal(input);
        } catch (final NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            Log.e(STUSecurityManager.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public byte[] encryptViaPubCert(final byte[] input) {
        try {
            final Cipher c = Cipher.getInstance(CIPHER_STR);
            c.init(Cipher.ENCRYPT_MODE, this.simBoxApiPubCert);
            return c.doFinal(input);
        } catch (final NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | IllegalBlockSizeException | BadPaddingException e) {
            Log.e(STUSecurityManager.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
