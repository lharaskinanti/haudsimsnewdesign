package com.haud.stu.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.haud.stu.android.BuildConfig;
import com.haud.stu.android.receivers.AlarmReceiver;

public class STUAlarmManager {
    public static synchronized void init(final Context context) {
        final AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        final boolean alreadySet =
                PendingIntent.getBroadcast(context, 0, intent,
                        PendingIntent.FLAG_NO_CREATE) != null;
        if (alreadySet) {
            Log.d(STUAlarmManager.class.getSimpleName(), "Alarm already set.");
            return;
        }
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                0, BuildConfig.API_KEEPALIVE_POLL_INTERVAL, alarmIntent);
        Log.d(STUAlarmManager.class.getSimpleName(), "Alarm initialised.");
    }
}
