package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SmsFromInbox {
    @SerializedName("address")
    private String address;
    @SerializedName("body")
    private String body;
    @SerializedName("sim_imsi")
    private String sim_imsi;
    @SerializedName("sim_slot")
    private String sim_slot;
    @SerializedName("threadId")
    private String threadId;


    public void setAddress(String address) { this.address = address; }
    public void setSim_slot(String sim_slot) { this.sim_slot = sim_slot; }
    public void setSim_imsi(String sim_imsi) { this.sim_imsi = sim_imsi; }
    public void setBody(String body) { this.body = body; }
    public void setThreadId(String threadId) { this.threadId = threadId; }


    public String getAddress() { return address; }
    public String getSim_imsi() { return sim_imsi; }
    public String getSim_slot() { return sim_slot; }
    public String getBody() { return body; }
    public String getThreadId() { return threadId; }


    @Override
    public String toString() {
        return "body: " + this.getBody() +
                ", sim_slot: " + this.getSim_slot() +
                ", sim_imsi: " + this.getSim_imsi() +
                ", threadId: " + this.getThreadId() +
                ", address: " + this.getAddress();
    }


}
