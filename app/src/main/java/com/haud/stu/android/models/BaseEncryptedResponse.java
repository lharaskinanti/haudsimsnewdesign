package com.haud.stu.android.models;

import com.google.gson.annotations.SerializedName;

public class BaseEncryptedResponse {

    @SerializedName("response_id")
    private String responseId;
    @SerializedName("data")
    private String data;

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
