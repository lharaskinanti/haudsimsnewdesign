package com.haud.stu.android;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.haud.stu.android.SMSManager;

import com.haud.stu.android.listeners.LocationListener;
import com.haud.stu.android.listeners.PhoneSignalListener;
import com.haud.stu.android.utils.GeneralUtils;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class Tab1Fragment extends Fragment {

    //private Context context;
    private View rootView;
    private static final AtomicBoolean DEVICE_INFO_LOADING = new AtomicBoolean(false);
    private Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //context = container.getContext();
        rootView = inflater.inflate(R.layout.fragment_one, container, false);
        if (BuildConfig.SHOW_DEBUG_DATA_MAIN_ACTIVITY) {
            PermissionsManager.checkPermissions(getActivity());
            LocationListener.init(getActivity(), new Runnable() {
                @Override
                public void run() {
                    buildDeviceInfo(null);
                }
            });
            PhoneSignalListener.init(getActivity(), new Runnable() {
                        @Override
                        public void run() {
                            buildDeviceInfo(null);
                        }
                    },
                    GeneralUtils.getSubIds(getActivity()));

            buildClickEvents();
            DeviceManager.doPrechecks(getActivity());
            buildDeviceInfo(null);
        } else {
            buildClickEvents();
        }
        return rootView;
        //return inflater.inflate(R.layout.fragment_one, container, false);
    }

    private void buildClickEvents() {
        //final Context ctx = this.context;
        final Button btnCopy = (Button) rootView.findViewById(R.id.btnCopyDeviceInfo);
        //final Tab1Fragment currActivity = this;
        btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager manager =
                        (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(
                        R.string.clipboard_copy_device_info_title),
                        getClipboardDeviceInfo());
                manager.setPrimaryClip(clip);
                Toast.makeText(getActivity(), getString(R.string.copied_to_clipboard),
                        Toast.LENGTH_SHORT).show();
            }
        });
        final Button btnRefresh = (Button) rootView.findViewById(R.id.btnRefreshDeviceInfo);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildDeviceInfo(getString(R.string.refreshed));
            }
        });
        final FloatingActionButton fabNeWSms = (FloatingActionButton) rootView.findViewById(R.id.fabNewSms);
        fabNeWSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ComposeSmsActivity.class);
                startActivity(intent);
            }
        });

        final Button pushSms = (Button) rootView.findViewById(R.id.btnPushsms);
        pushSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushSms();
            }
        });

    }


    private String getClipboardDeviceInfo() {
        Context ctx = getActivity();
        final Map<String, String> info = GeneralUtils.getDeviceInfo(ctx);
        final StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> e : info.entrySet()) {
            final String key = e.getKey();
            final String value = e.getValue();
            sb.append(key);
            sb.append(": ");
            sb.append(value);
            sb.append("\n");
        }
        return sb.toString();
    }


    /**
     * @param toastCompleteText If not null, a toast is shown with this String as the text,
     *                          once it completes its job.
     */
    private void buildDeviceInfo(final String toastCompleteText) {
        if (!DEVICE_INFO_LOADING.getAndSet(true)) {
            //mHandler.post(new Runnable() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Context ctx = getActivity();

                    if(getActivity() == null)
                        return;

                        final Map<String, String> deviceInfo = GeneralUtils.getDeviceInfo(ctx);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final TableLayout tl = (TableLayout) rootView.findViewById(R.id.tableDeviceInfo);
                                tl.removeAllViews();
                                tl.addView(getActiveStatusRow());
                                for (Map.Entry<String, String> kvp : deviceInfo.entrySet()) {
                                    final TextView title = getTableRowTitle(kvp.getKey());
                                    final TextView value = getTableRowValue(kvp.getValue());
                                    final TableRow row = new TableRow(ctx);
                                    row.addView(title);
                                    row.addView(value);
                                    tl.addView(row);
                                }
                                if (toastCompleteText != null) {
                                    Toast.makeText(ctx, toastCompleteText, Toast.LENGTH_SHORT).show();
                                }
                                DEVICE_INFO_LOADING.set(false);
                            }
                        });
                    }

            }).start();
        }
    }

    private View getActiveStatusRow() {
        final Context ctx = getActivity();
        final boolean active = DeviceManager.isActive();
        final TextView title = new TextView(ctx);
        title.setText(getString(R.string.device_active));
        title.setTypeface(null, Typeface.BOLD);
        final TextView value = new TextView(ctx);
        value.setText(active ? getString(R.string.yes) : getString(R.string.no));
        value.setTextColor(active ? Color.GREEN : Color.RED);
        final TableRow row = new TableRow(ctx);
        row.addView(title);
        row.addView(value);
        return row;
    }

    private TextView getTableRowTitle(final String title) {
        final Context ctx = getActivity();
        final TextView tvTitle = new TextView(ctx);
        tvTitle.setTypeface(null, Typeface.BOLD);
        tvTitle.setText(String.format("%s:", title));
        return tvTitle;
    }

    private TextView getTableRowValue(final String value) {
        final Context ctx = getActivity();
        final TextView tvValue = new TextView(ctx);
        tvValue.setText(value);
        return tvValue;
    }

    private void pushSms() {
        final Context ctx = getActivity();
        SMSManager.processPendingSMSes(ctx);
    }



}
