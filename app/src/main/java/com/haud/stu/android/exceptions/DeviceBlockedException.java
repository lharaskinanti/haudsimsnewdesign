package com.haud.stu.android.exceptions;

public class DeviceBlockedException extends Exception {

    public DeviceBlockedException() {
    }

    public DeviceBlockedException(final String message) {
        super(message);
    }

    public DeviceBlockedException(final String message, final Throwable tr) {
        super(message, tr);
    }

    public DeviceBlockedException(final Throwable tr) {
        super(tr);
    }
}
