package com.haud.stu.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.UUID;

public class MmsReceiver  extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final String uuid = UUID.randomUUID().toString();
        final String action = intent.getAction();
        Log.i(MmsReceiver.class.getSimpleName(), String.format("UUID: [%s] - New Broadcast Received" +
                " with action [%s].", uuid, action));

    }
}
