package com.haud.stu.android.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.haud.stu.android.STUAlarmManager;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            return;
        }
        Log.i(BootReceiver.class.getSimpleName(), "Boot completed signal received.");
        STUAlarmManager.init(context);
    }
}
