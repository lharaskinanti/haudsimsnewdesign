package com.haud.stu.android.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v4.content.ContextCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.haud.stu.android.R;
import com.haud.stu.android.db.DBHelper;
import com.haud.stu.android.db.DBModel;
import com.haud.stu.android.listeners.LocationListener;
import com.haud.stu.android.listeners.PhoneSignalListener;
import com.haud.stu.android.models.SmsFromInbox;
import com.haud.stu.android.models.SmsFromStorage;
import com.haud.stu.android.models.StatusRequest;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import com.stericson.RootShell.RootShell;

public class GeneralUtils {

    public static class SimSubscriberInfo {
        private final int slot;
        private final String imei;
        private final String phoneNumber;
        private final String imsi;
        private final int mcc;
        private final int mnc;
        private final String simCarrierName;
        private final boolean roaming;
        private final String cardid;

        public SimSubscriberInfo(int slot, String imei, String phoneNumber, String imsi,
                                 int mcc, int mnc, String simCarrierName, boolean roaming, String cardid) {
            this.slot = slot;
            this.cardid = cardid;
            this.imei = imei != null && !imei.trim().isEmpty() ? imei.trim() : null;
            this.phoneNumber = phoneNumber != null && !phoneNumber.trim().isEmpty() ?
                    phoneNumber.trim() : null;
            this.mcc = mcc;
            this.mnc = mnc;
            this.imsi = imsi != null && !imsi.trim().isEmpty() ? imsi.trim() : null;
            this.simCarrierName = simCarrierName != null &&
                    !simCarrierName.trim().isEmpty() ? simCarrierName
                    .trim() : null;
            this.roaming = roaming;
        }

        public int getSlot() {
            return slot;
        }

        public String getImei() {
            return imei;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getImsi() {
            return imsi;
        }

        public int getMcc() {
            return mcc;
        }

        public int getMnc() {
            return mnc;
        }

        public String getSimCarrierName() {
            return simCarrierName;
        }

        public boolean isRoaming() {
            return roaming;
        }

        public String getIccId() { return cardid; }
    }

    public static boolean isDeviceRooted() {
        //return RootShell.isRootAvailable() || RootShell.isAccessGiven();
        return true;
    }

    /**
     * Returns the WiFi signal strength as a percentage.
     *
     * @param ctx
     * @return WiFi Signal Strength as a percentage. 0 if unavailable or no permissions.
     */
    public static int getWifiSignalStrength(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_WIFI_STATE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
            return 0;
        }
        WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo == null) {
            return 0;
        }
        wifiInfo.getRssi();
        return WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 100);
    }

    public static Map<String, String> getDeviceInfo(final Context ctx) {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

        PackageInfo pInfo = null;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(GeneralUtils.class.getSimpleName(), "Unable to get app info.", e);
        }
        String version = pInfo == null ? ctx.getString(R.string.unknown) : pInfo.versionName;
        final GeneralUtils.UnknownBoolean multiSimSupport = hasMultiSimSupport(ctx);
        final GeneralUtils.UnknownBoolean hasMultiSim = hasMultiSim(ctx);
        final NetworkInfo activeNetwork = getActiveNetworkInfo(ctx);
        final Location latestLocation = LocationListener.getLatestLocation(ctx);
        final String latestLocationStr = latestLocation == null ? ctx.getString(R.string.unknown)
                : String.format("%s, %s", latestLocation.getLatitude(), latestLocation.getLongitude());
        final String latestLocationUpdated = latestLocation == null ? ctx.getString(R.string.unknown)
                : String.valueOf(latestLocation.getTime());
        final String locationServiceEnabled = LocationListener.isLocationServiceEnabled(ctx) ?
                ctx.getString(R.string.yes) : ctx.getString(R.string.no);
        final PhoneSignalListener.SignalStrengthInfo latestSignalStrength =
                PhoneSignalListener.getLatestStrengthInfo(0, ctx);
        final String latestSignalStrengthStr = latestSignalStrength != null ?
                String.format("%d%% (%S)", latestSignalStrength.getLevelPercentage(),
                        latestSignalStrength.getSignalType()) :
                ctx.getString(R.string.unknown);
        final GeneralUtils.UnknownBoolean isDefaultMsgingApp = isDefaultMessagingApp(ctx);
        final String isDefaultMsgingAppStr = isDefaultMsgingApp == UnknownBoolean.TRUE ?
                ctx.getString(R.string.yes) : isDefaultMsgingApp == UnknownBoolean.FALSE ?
                ctx.getString(R.string.no) : ctx.getString(R.string.n_a);


        String android_id = Settings.Secure.getString(ctx.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("Android","Android ID : "+android_id);

        String android_serial = Build.SERIAL == "unknown" ? android_id : Build.SERIAL;

        result.put(ctx.getString(R.string.device_model), String.format("%s %s",
                Build.MANUFACTURER, Build.MODEL));

        //result.put(ctx.getString(R.string.device_serial), Build.SERIAL);
        result.put(ctx.getString(R.string.device_serial), android_serial);



        result.put(ctx.getString(R.string.android_version), String.format("%s (%s %d)",
                Build.VERSION.RELEASE, ctx.getString(R.string.api_level), Build.VERSION.SDK_INT));
        result.put(ctx.getString(R.string.app_version), version);
        result.put(ctx.getString(R.string.rooted), isDeviceRooted() ?
                ctx.getString(R.string.yes) : ctx.getString(R.string.no));
        result.put(ctx.getString(R.string.default_msg_app), isDefaultMsgingAppStr);
        result.put(ctx.getString(R.string.has_multisim_support),
                multiSimSupport == GeneralUtils.UnknownBoolean.TRUE
                        ? ctx.getString(R.string.yes) : multiSimSupport == GeneralUtils.UnknownBoolean.FALSE ?
                        ctx.getString(R.string.no) : ctx.getString(R.string.unknown));
        result.put(ctx.getString(R.string.has_multisim),
                hasMultiSim == GeneralUtils.UnknownBoolean.TRUE
                        ? ctx.getString(R.string.yes) : hasMultiSim == GeneralUtils.UnknownBoolean.FALSE ?
                        ctx.getString(R.string.no) : ctx.getString(R.string.unknown));
        result.put(ctx.getString(R.string.sim_count), String.valueOf(getSimCount(ctx)));
        final List<SimSubscriberInfo> simInfos = getSimSubscriptionInfos(ctx);
        final String slotToken = "[SLOT]";
        for (int i = 0; i < simInfos.size(); i++) {

            final SimSubscriberInfo simInfo = simInfos.get(i);
            final String slot = String.valueOf(simInfo.getSlot() + 1);
            final String carrierName = simInfo.getSimCarrierName() == null ?
                    ctx.getString(R.string.unknown) : simInfo.getSimCarrierName();
            final String phoneNum = simInfo.getPhoneNumber() == null ?
                    ctx.getString(R.string.unknown) : simInfo.getPhoneNumber();
            final String roaming = simInfo.isRoaming() ? ctx.getString(R.string.yes) :
                    ctx.getString(R.string.no);


            result.put(ctx.getString(R.string.sim_x_imei).replace(slotToken, slot), simInfo.getImei());
            result.put(ctx.getString(R.string.sim_x_imsi).replace(slotToken, slot), simInfo.getImsi());
            result.put(ctx.getString(R.string.sim_x_cardid).replace(slotToken, slot), simInfo.getIccId());
            //getCardId
            result.put(ctx.getString(R.string.sim_x_mcc).replace(slotToken, slot),
                    String.valueOf(simInfo.getMcc()));
            result.put(ctx.getString(R.string.sim_x_mnc).replace(slotToken, slot),
                    String.valueOf(simInfo.getMnc()));

            result.put(ctx.getString(R.string.sim_x_carriername).replace(slotToken, slot),
                    carrierName);
            result.put(ctx.getString(R.string.sim_x_number).replace(slotToken, slot), phoneNum);
            result.put(ctx.getString(R.string.sim_x_roaming).replace(slotToken, slot), roaming);
        }
        result.put(ctx.getString(R.string.wifi_enabled), isWifiEnabled(ctx) ?
                ctx.getString(R.string.yes) : ctx.getString(R.string.no));
        result.put(ctx.getString(R.string.wifi_signal_strength), String.format("%d%%",
                getWifiSignalStrength(ctx)));
        result.put(ctx.getString(R.string.active_network),
                getActiveNetworkInfoText(activeNetwork, ctx));
        result.put(ctx.getString(R.string.network_connected), activeNetwork == null ?
                ctx.getString(R.string.no) : activeNetwork.isConnected() ?
                ctx.getString(R.string.yes) : ctx.getString(R.string.no));
        result.put(ctx.getString(R.string.location_enabled), locationServiceEnabled);
        result.put(ctx.getString(R.string.location), latestLocationStr);
        result.put(ctx.getString(R.string.location_last_fix), latestLocationUpdated);
        result.put(ctx.getString(R.string.mobile_signal_strength), latestSignalStrengthStr);
        return result;
    }

    public static boolean isCharging(final Context context) {
        Intent batteryIntent = context.getApplicationContext().registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        return status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
    }

    public static double getBatteryLevel(final Context context) {
        Intent batteryIntent = context.getApplicationContext().registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level == -1 || scale == -1) {
            return 50.0f;
        }
        return ((float) level / (float) scale) * 100.0f;
    }

    public static String getWiFiSsid(final Context ctx) {
        final NetworkInfo activeNetwork = getActiveNetworkInfo(ctx);
        if (activeNetwork == null) {
            return null;
        }
        if (activeNetwork.getType() != ConnectivityManager.TYPE_WIFI) {
            return null;
        }
        final WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager == null) {
            return null;
        }
        final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        return connectionInfo.getSSID();
    }

    private static String getActiveNetworkInfoText(NetworkInfo activeNetwork, Context ctx) {
        if (activeNetwork == null) {
            return ctx.getString(R.string.none);
        }
        String type = activeNetwork.getTypeName();
        String subType = activeNetwork.getSubtypeName();
        StringBuilder sb = new StringBuilder();
        sb.append(type);
        if (subType != null) {
            sb.append(" - ");
            sb.append(subType);
        }
        if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
            final WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null) {
                sb.append(ctx.getString(R.string.ssid));
                sb.append(": ");
                sb.append(connectionInfo.getSSID());
            }
        }
        return sb.toString();
    }

    public enum UnknownBoolean {
        TRUE, FALSE, UNKNOWN
    }

    public static GeneralUtils.UnknownBoolean isDefaultMessagingApp(final Context ctx) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return GeneralUtils.UnknownBoolean.UNKNOWN;
        }
        return ctx.getPackageName().equals(Telephony.Sms.getDefaultSmsPackage(ctx))
                ? GeneralUtils.UnknownBoolean.TRUE : GeneralUtils.UnknownBoolean.FALSE;
    }

    public static UnknownBoolean hasMultiSimSupport(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return UnknownBoolean.UNKNOWN;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return UnknownBoolean.UNKNOWN;
        }
        SubscriptionManager sm = (SubscriptionManager)
                ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        return sm.getActiveSubscriptionInfoCountMax() > 1 ? UnknownBoolean.TRUE : UnknownBoolean.FALSE;
    }

    public static UnknownBoolean hasMultiSim(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return UnknownBoolean.UNKNOWN;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return UnknownBoolean.UNKNOWN;
        }
        SubscriptionManager sm = (SubscriptionManager)
                ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        return sm.getActiveSubscriptionInfoCount() > 1 ? UnknownBoolean.TRUE : UnknownBoolean.FALSE;
    }

    public static int[] getSubIds(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return new int[0];
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            final TelephonyManager telMgr = (TelephonyManager) ctx.getSystemService(
                    Context.TELEPHONY_SERVICE);
            final int simState = telMgr.getSimState();
            if (simState == TelephonyManager.SIM_STATE_ABSENT) {
                return new int[0];
            }
            return new int[]{0};
        } else {
            SubscriptionManager sm = (SubscriptionManager)
                    ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> subInfos = sm.getActiveSubscriptionInfoList();
            int[] result = new int[subInfos.size()];
            for (int i = 0; i < subInfos.size(); i++) {
                SubscriptionInfo subInfo = subInfos.get(i);
                result[i] = subInfo.getSimSlotIndex();
            }
            for (int slot : result) {
                Log.d(GeneralUtils.class.getSimpleName(), String.format("Slot found: %d", slot));
            }
            return result;
        }
    }

    public static int getSimCount(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return 0;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            final TelephonyManager telMgr = (TelephonyManager) ctx.getSystemService(
                    Context.TELEPHONY_SERVICE);
            final int simState = telMgr.getSimState();
            if (simState == TelephonyManager.SIM_STATE_ABSENT) {
                return 0;
            }
            return 1;
        } else {
            SubscriptionManager sm = (SubscriptionManager)
                    ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            return sm.getActiveSubscriptionInfoCount();
        }
    }

    public static boolean isRoamingForSlot(final TelephonyManager telephonyManager,
                                           final int slotId) {
        boolean roaming = false;
        try {
            Method m = TelephonyManager.class.getMethod("isNetworkRoaming", int.class);
            roaming = (boolean) m.invoke(telephonyManager, slotId);
        } catch (Exception e) {
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
        }
        return roaming;
    }

    public static String getDefaultImei(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static String getImeiForSlot(final int slotId, final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = getImeiForSlot(telephonyManager, slotId);
        if (imei == null) {
            imei = getDefaultImei(ctx);
        }
        return imei;
    }

    public static String getImeiForSlot(final TelephonyManager telephonyManager, final int slotId) {
        String imei = null;
        try {
            Method m = TelephonyManager.class.getMethod("getDeviceId", int.class);
            imei = (String) m.invoke(telephonyManager, slotId);
        } catch (Exception e) {
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
        }
        return imei;
    }

    public static double getAppVersion(final Context ctx) {
        PackageInfo pInfo;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return pInfo.versionCode;
    }

    public static String getAppVersionStr(final Context ctx) {
        PackageInfo pInfo;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return pInfo.versionName;
    }

    public static boolean isWifiEnabled(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_WIFI_STATE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        WifiManager wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        return wifi == null ? false : wifi.isWifiEnabled();
    }

    public static NetworkInfo getActiveNetworkInfo(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_WIFI_STATE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        ConnectivityManager connManager = (ConnectivityManager) ctx.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        return connManager.getActiveNetworkInfo();
    }

    public static String getNetworkOperatorForSlot(final TelephonyManager telephonyManager,
                                                   final int slotId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            return telephonyManager.getNetworkOperator();
        }
        try {
            Method mNetOp = TelephonyManager.class.getMethod("getNetworkOperatorForPhone", int.class);
            String mccMnc = (String) mNetOp.invoke(telephonyManager, slotId);
            Log.d(GeneralUtils.class.getSimpleName(),
                    String.format("Found MCC MNC [%s] for Phone ID [%d].", mccMnc, slotId));
            return mccMnc;
        } catch (Exception e) {
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
        }
        return null;
    }


    public static String getImsiForSlot(final int slotId, final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        return getImsiForSlot(telephonyManager, slotId);
    }
    

    public static String getImsiForSlot(final TelephonyManager telephonyManager, final int slotId) {
        String imsi = null;
        try {
            Method m = TelephonyManager.class.getMethod("getSubscriberId", int.class);
            imsi = (String) m.invoke(telephonyManager, slotId);
        } catch (Exception e) {
            //assume it is a single-sim device since it doesn't have support for the above so fallback.
            Log.e(GeneralUtils.class.getSimpleName(), e.getMessage(), e);
            //imsi = telephonyManager.getSubscriberId();
            imsi = null;
        }
        return imsi;
    }

    public static String getIccidForSlot(final int slotId, final Context ctx) {
        String iccid = null;

        final List<SimSubscriberInfo> simInfos = getSimSubscriptionInfos(ctx);
        final SimSubscriberInfo simInfo = simInfos.get(slotId);
        iccid = simInfo.getIccId();
        return iccid;
    }


    public static StatusRequest.Network[] getNetworkInfo(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return new StatusRequest.Network[0];
        }
        List<StatusRequest.Network> result = new ArrayList<>();
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        final String[] mccMncs;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            mccMncs = new String[1];
            mccMncs[0] = telephonyManager.getNetworkOperator();
        } else {
            SubscriptionManager sm = (SubscriptionManager)
                    ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            final int slotCount = sm.getActiveSubscriptionInfoCountMax();
            mccMncs = new String[slotCount];
            List<SimSubscriberInfo> subInfos = getSimSubscriptionInfos(ctx);
            for (SimSubscriberInfo si : subInfos) {
                mccMncs[si.getSlot()] = getNetworkOperatorForSlot(telephonyManager, si.getSlot());
            }
        }
        for (int i = 0; i < mccMncs.length; i++) {
            String mccMnc = mccMncs[i];
            int mcc = 0;
            int mnc = 0;
            if (mccMnc != null && mccMnc.length() >= 5) {
                mcc = Integer.parseInt(mccMnc.substring(0, 3));
                mnc = Integer.parseInt(mccMnc.substring(3));
            }
            StatusRequest.Network n = new StatusRequest.Network();
            PhoneSignalListener.SignalStrengthInfo signalInfo =
                    PhoneSignalListener.getLatestStrengthInfo(i, ctx);
            n.setSlot(i);
            n.setMcc(mcc);
            n.setMnc(mnc);
            if (signalInfo != null) {
                n.setSignalStrength((double) signalInfo.getLevelPercentage());
                n.setNetworkType(signalInfo.getSignalType());
            }
            result.add(n);
        }
        return result.toArray(new StatusRequest.Network[result.size()]);
    }

    public static List<SimSubscriberInfo> _getSimSubscriptionInfos(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return new ArrayList<>();
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        List<SimSubscriberInfo> result = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager sm = (SubscriptionManager)
                    ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> infos = sm.getActiveSubscriptionInfoList();
            final int simCount = getSimCount(ctx);
            for (SubscriptionInfo info : infos) {
                final String imei;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    imei = getImeiForSlot(telephonyManager, info.getSimSlotIndex());
                } else {
                    imei = simCount == 1 ? telephonyManager.getDeviceId() :
                            ctx.getString(R.string.unknown);
                }

                final String cardid = info.getIccId();

                final int mcc = info.getMcc();
                final int mnc = info.getMnc();
                String msisdn = info.getNumber();
                msisdn = msisdn.replaceAll("\\D+","");

                String smcc = Integer.toString(mcc);
                String smnc = Integer.toString(mnc);
                String tmnc = ("00" + smnc).substring(smnc.length());
                String mccmncNumber = smcc + tmnc + msisdn;

                result.add(new SimSubscriberInfo(info.getSimSlotIndex(),
                        imei,
                        info.getNumber(),
                        mccmncNumber,
                        info.getMcc(), info.getMnc(), info.getCarrierName().toString(),
                        isRoamingForSlot(telephonyManager, info.getSimSlotIndex()), cardid));
            }
        } else if (telephonyManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            String netOperator = telephonyManager.getSimOperator();
            String mccStr = netOperator != null && netOperator.length() >= 5 ?
                    netOperator.substring(0, 3) : null;

            String mncStr = netOperator != null && netOperator.length() >= 5 ?
                    netOperator.substring(3) : null;
            int mcc = 0;
            int mnc = 0;
            if (mccStr.matches("[0-9]{1,3}")) {
                mcc = Integer.parseInt(mccStr);
            }
            if (mncStr.matches("[0-9]{1,3}")) {
                mnc = Integer.parseInt(mncStr);
            }
            result.add(new SimSubscriberInfo(0, telephonyManager.getDeviceId(),
                    telephonyManager.getLine1Number(),
                    telephonyManager.getSubscriberId(),
                    mcc, mnc, telephonyManager.getSimOperatorName(),
                    telephonyManager.isNetworkRoaming(), null));
        }
        return result;
    }

    public static List<SimSubscriberInfo> getSimSubscriptionInfos(final Context ctx) {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            return new ArrayList<>();
        }
        TelephonyManager telephonyManager = (TelephonyManager)
                ctx.getSystemService(Context.TELEPHONY_SERVICE);
        List<SimSubscriberInfo> result = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager sm = (SubscriptionManager)
                    ctx.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> infos = sm.getActiveSubscriptionInfoList();
            final int simCount = getSimCount(ctx);
            for (SubscriptionInfo info : infos) {
                final String imei;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    imei = getImeiForSlot(telephonyManager, info.getSimSlotIndex());
                } else {
                    imei = simCount == 1 ? telephonyManager.getDeviceId() :
                            ctx.getString(R.string.unknown);
                }

                final String iccid = info.getIccId();

                result.add(new SimSubscriberInfo(info.getSimSlotIndex(),
                        imei,
                        info.getNumber(),
                        getImsiForSlot(telephonyManager, info.getSimSlotIndex()),
                        info.getMcc(), info.getMnc(), info.getCarrierName().toString(),
                        isRoamingForSlot(telephonyManager, info.getSimSlotIndex()), iccid));
            }
        } else if (telephonyManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            String netOperator = telephonyManager.getSimOperator();
            String mccStr = netOperator != null && netOperator.length() >= 5 ?
                    netOperator.substring(0, 3) : null;
            String mncStr = netOperator != null && netOperator.length() >= 5 ?
                    netOperator.substring(3) : null;

            //final String iccid = telephonyManager.getIccId();

            int mcc = 0;
            int mnc = 0;
            if (mccStr.matches("[0-9]{1,3}")) {
                mcc = Integer.parseInt(mccStr);
            }
            if (mncStr.matches("[0-9]{1,3}")) {
                mnc = Integer.parseInt(mncStr);
            }
            result.add(new SimSubscriberInfo(0, telephonyManager.getDeviceId(),
                    telephonyManager.getLine1Number(),
                    telephonyManager.getSubscriberId(),
                    mcc, mnc, telephonyManager.getSimOperatorName(),
                    telephonyManager.isNetworkRoaming(), null));
        }
        return result;
    }



    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }




}
