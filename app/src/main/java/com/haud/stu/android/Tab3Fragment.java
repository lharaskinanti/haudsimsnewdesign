package com.haud.stu.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.haud.stu.android.models.SmsFromStorage;
import com.haud.stu.android.utils.SmslistsUtils;

import java.util.ArrayList;
import java.util.List;

public class Tab3Fragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        final int slot = 1;
        getSmsfromSlot(slot);
        return rootView;
    }

    private void getSmsfromSlot(final int slot) {
        final Context ctx = getActivity();
        System.out.println("Tab three calling sms");
        final List<SmsFromStorage> smsall = SmslistsUtils.listStorageSMSes(ctx, 1);
        System.out.println(smsall);
        ArrayAdapter<SmsFromStorage> adapter = new StorageSmsAdapter(ctx, 0, (ArrayList<SmsFromStorage>) smsall);
        ListView listView = (ListView) rootView.findViewById(R.id.customListView);
        listView.setAdapter(adapter);

        AdapterView.OnItemClickListener adapterViewListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SmsFromStorage sms = smsall.get(position);
                Intent intent = new Intent(getActivity(), SmsFromOa.class);
                intent.putExtra("from", sms.getAddress());
                intent.putExtra("slot", slot);
                startActivityForResult(intent, 1000);
            }
        };

        listView.setOnItemClickListener(adapterViewListener);

    }

}





