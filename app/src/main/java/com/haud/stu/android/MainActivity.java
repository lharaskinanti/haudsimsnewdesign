package com.haud.stu.android;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.haud.stu.android.BuildConfig;
import com.haud.stu.android.R;
import com.haud.stu.android.listeners.LocationListener;
import com.haud.stu.android.listeners.PhoneSignalListener;
import com.haud.stu.android.utils.GeneralUtils;
import com.haud.stu.android.SMSManager;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

public class MainActivity extends AppCompatActivity {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private static final AtomicBoolean DEVICE_INFO_LOADING = new AtomicBoolean(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.SHOW_DEBUG_DATA_MAIN_ACTIVITY) {
            setContentView(R.layout.activity_main);

            viewPager = (ViewPager) findViewById(R.id.viewPager);
            tabLayout = (TabLayout) findViewById(R.id.tabLayout);
            adapter = new TabAdapter(getSupportFragmentManager());
            adapter.addFragment(new Tab1Fragment(), "Device");
            adapter.addFragment(new Tab2Fragment(), "Sim 1");
            adapter.addFragment(new Tab3Fragment(), "Sim 2");
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);

//            PermissionsManager.checkPermissions(this);
//            LocationListener.init(this, new Runnable() {
//                @Override
//                public void run() {
//                    buildDeviceInfo(null);
//                }
//            });
//            PhoneSignalListener.init(this, new Runnable() {
//                        @Override
//                        public void run() {
//                            buildDeviceInfo(null);
//                        }
//                    },
//                    GeneralUtils.getSubIds(this));
//            buildClickEvents();
//            DeviceManager.doPrechecks(this);
//            buildDeviceInfo(null);
        } else {
            setContentView(R.layout.activity_main_empty);
            PermissionsManager.checkPermissions(this);
            LocationListener.init(this, new Runnable() {
                @Override
                public void run() {

                }
            });
            PhoneSignalListener.init(this, new Runnable() {
                @Override
                public void run() {
                }
            },
            GeneralUtils.getSubIds(this));
            DeviceManager.doPrechecks(this);
        }

    }

    private void buildClickEvents() {
        final Button btnCopy = (Button) findViewById(R.id.btnCopyDeviceInfo);
        final Activity currActivity = this;
        btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager manager =
                        (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(
                        R.string.clipboard_copy_device_info_title),
                        getClipboardDeviceInfo());
                manager.setPrimaryClip(clip);
                Toast.makeText(currActivity, getString(R.string.copied_to_clipboard),
                        Toast.LENGTH_SHORT).show();
            }
        });
        final Button btnRefresh = (Button) findViewById(R.id.btnRefreshDeviceInfo);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildDeviceInfo(getString(R.string.refreshed));
            }
        });
        final FloatingActionButton fabNeWSms = (FloatingActionButton) findViewById(R.id.fabNewSms);
        fabNeWSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(currActivity, ComposeSmsActivity.class);
                startActivity(intent);
            }
        });

        final Button pushSms = (Button) findViewById(R.id.btnPushsms);
        pushSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushSms();
            }
        });

    }


    private void pushSms() {
        final Context ctx = this;
        SMSManager.processPendingSMSes(ctx);
    }

    private String getClipboardDeviceInfo() {
        final Map<String, String> info = GeneralUtils.getDeviceInfo(this);
        final StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> e : info.entrySet()) {
            final String key = e.getKey();
            final String value = e.getValue();
            sb.append(key);
            sb.append(": ");
            sb.append(value);
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * @param toastCompleteText If not null, a toast is shown with this String as the text,
     *                          once it completes its job.
     */
    private void buildDeviceInfo(final String toastCompleteText) {
        final Context ctx = this;
        if (!DEVICE_INFO_LOADING.getAndSet(true)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Map<String, String> deviceInfo = GeneralUtils.getDeviceInfo(ctx);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            final TableLayout tl = (TableLayout) findViewById(R.id.tableDeviceInfo);
                            tl.removeAllViews();
                            tl.addView(getActiveStatusRow());
                            for (Map.Entry<String, String> kvp : deviceInfo.entrySet()) {
                                final TextView title = getTableRowTitle(kvp.getKey());
                                final TextView value = getTableRowValue(kvp.getValue());
                                final TableRow row = new TableRow(ctx);
                                row.addView(title);
                                row.addView(value);
                                tl.addView(row);
                            }
                            if (toastCompleteText != null) {
                                Toast.makeText(ctx, toastCompleteText, Toast.LENGTH_SHORT).show();
                            }
                            DEVICE_INFO_LOADING.set(false);
                        }
                    });
                }
            }).start();
        }
    }

    private View getActiveStatusRow() {
        final boolean active = DeviceManager.isActive();
        final TextView title = new TextView(this);
        title.setText(getString(R.string.device_active));
        title.setTypeface(null, Typeface.BOLD);
        final TextView value = new TextView(this);
        value.setText(active ? getString(R.string.yes) : getString(R.string.no));
        value.setTextColor(active ? Color.GREEN : Color.RED);
        final TableRow row = new TableRow(this);
        row.addView(title);
        row.addView(value);
        return row;
    }

    private TextView getTableRowTitle(final String title) {
        final TextView tvTitle = new TextView(this);
        tvTitle.setTypeface(null, Typeface.BOLD);
        tvTitle.setText(String.format("%s:", title));
        return tvTitle;
    }

    private TextView getTableRowValue(final String value) {
        final TextView tvValue = new TextView(this);
        tvValue.setText(value);
        return tvValue;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        PermissionsManager.handlePermissionsResult(requestCode, permissions, grantResults, this);
    }
}
