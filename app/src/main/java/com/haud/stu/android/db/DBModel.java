package com.haud.stu.android.db;

import android.provider.BaseColumns;

public class DBModel {
    static abstract class Indexes {
        public static final String IDX_SMSENTRY_TIMESTAMP = "idx_smsentry_timestamp";
    }

    public static abstract class SMSEntry implements BaseColumns {
        public static final String TABLE_NAME = "sms_entry";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_EVENT_UUID = "event_uuid";
        public static final String COLUMN_NAME_PDU_FORMAT = "pdu_format";
        public static final String COLUMN_NAME_SIM_SLOT = "sim_slot";
        public static final String COLUMN_NAME_SIM_IMSI = "sim_imsi";
        public static final String COLUMN_NAME_SIM_ICCID = "sim_iccid";
        public static final String COLUMN_NAME_STATUS = "status";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_BODY = "body";
    }

    public static abstract class SMSEntryPdu implements BaseColumns {
        public static final String TABLE_NAME = "sms_entry_pdu";
        public static final String COLUMN_NAME_SMS_ENTRY_ID = "sms_entry_id";
        public static final String COLUMN_NAME_PDU = "pdu";
    }

    static final String SQL_CREATE_SMS_ENTRY = String.format("CREATE TABLE `%s` (" +
                    "`%s` INTEGER PRIMARY KEY NOT NULL, " +
                    "`%s` INTEGER NOT NULL, " +
                    "`%s` VARCHAR(36) NOT NULL, " +
                    "`%s` VARCHAR(64) NULL, " +
                    "`%s` INTEGER NOT NULL, " +
                    "`%s` VARCHAR (36) NULL, " +
                    "`%s` INTEGER NOT NULL DEFAULT 0, " +
                    "`%s` TEXT NULL, " +
                    "`%s` VARCHAR(64) NULL, " +
                    "`%s` VARCHAR(64) NULL)", SMSEntry.TABLE_NAME, SMSEntry._ID,
            SMSEntry.COLUMN_NAME_TIMESTAMP, SMSEntry.COLUMN_NAME_EVENT_UUID,
            SMSEntry.COLUMN_NAME_PDU_FORMAT, SMSEntry.COLUMN_NAME_SIM_SLOT,
            SMSEntry.COLUMN_NAME_ADDRESS, SMSEntry.COLUMN_NAME_STATUS,
            SMSEntry.COLUMN_NAME_BODY,
            SMSEntry.COLUMN_NAME_SIM_IMSI, SMSEntry.COLUMN_NAME_SIM_ICCID);

    static final String SQL_CREATE_SMS_ENTRY_IDX_TIME = String.format(
            "CREATE INDEX `%s` on `%s` (%s)", Indexes.IDX_SMSENTRY_TIMESTAMP,
            SMSEntry.TABLE_NAME, SMSEntry.COLUMN_NAME_TIMESTAMP);

    static final String SQL_CREATE_SMS_ENTRY_PDU = String.format("CREATE TABLE `%s` (" +
                    "`%s` INTEGER PRIMARY KEY NOT NULL, " +
                    "`%s` INTEGER NOT NULL, " +
                    "`%s` BLOB NOT NULL," +
                    "FOREIGN KEY (`%s`) REFERENCES `%s`(`%s`))", SMSEntryPdu.TABLE_NAME,
            SMSEntryPdu._ID,
            SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID, SMSEntryPdu.COLUMN_NAME_PDU,
            SMSEntryPdu.COLUMN_NAME_SMS_ENTRY_ID, SMSEntry.TABLE_NAME, SMSEntry._ID);

}
